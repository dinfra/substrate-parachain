# DINFRA Common Crate

Common definitions: traits, constants, types, etc shared across DINFRA pallets.

The subscriber pallet can make requests to the provider pallet through a loosely coupled integration pattern.
