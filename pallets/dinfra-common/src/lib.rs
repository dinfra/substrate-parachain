// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

#![cfg_attr(not(feature = "std"), no_std)]

use frame_support::{
	pallet_prelude::*,
	traits::{Currency, ReservableCurrency},
};

use sp_std::vec::Vec;

pub mod traits;

// TODO: Would it be better to create an OpaqueCID type? that can handle the type storage more efficiently...

/// IPFS Content ID Representation
pub type CID = Vec<u8>;

/// IPFS CIDs representation is versioned and could change in the future. DINFRA stores references
/// to IPFS as CIDs in binary, as they end up in the blockchain, they need to be limited to a Max Length
/// this trait creates common configuration attributes for dinfra pallets using CIDs.

pub trait DinfraConfig: frame_system::Config {
	/// The currency trait for DINFRA pallets
	type Currency: ReservableCurrency<Self::AccountId>;

	type CIDEncodingLimit: Get<u32>;
}

/// The actual maximun length recommended when configuring dinfra pallets.
/// Current IPFS CIDs are around 40bytes, and unlikely that future version will grow much larger.
pub const CID_ENCODING_LIMIT: u32 = 64;

/// Type used for a Stored CID
pub type CIDof<T> = BoundedVec<u8, <T as DinfraConfig>::CIDEncodingLimit>;

/// A Deployment Type is basically CID that can be Stored. the document holds a JSON Schema
/// used to validate deployment descriptors conforming to this type.
pub type DeploymentTypeOf<T> = CIDof<T>;

/// A Deployment Descriptor is basically CID that can be Stored. the document holds a JSON Object
/// that conforms with the requested deployment type.
pub type DeploymentDescriptorOf<T> = CIDof<T>;

/// The AccountId for DINFRA Pallets, and marker types
pub type AccountIdOf<T> = <T as frame_system::Config>::AccountId;
pub type ProviderAccountIdOf<T> = AccountIdOf<T>;
pub type SubscriberAccountIdOf<T> = AccountIdOf<T>;

/// The Balance for our Pallets

pub type BalanceOf<T> = <<T as DinfraConfig>::Currency as Currency<AccountIdOf<T>>>::Balance;

// Simple shared types

/// Deployment Types, human friendly identification.
pub type DeploymentTypeId = u32;
