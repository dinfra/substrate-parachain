// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

use codec::{Decode, Encode, MaxEncodedLen};
use frame_system::Config;
use scale_info::TypeInfo;
use sp_runtime::RuntimeDebug;
use sp_std::vec::Vec;

use crate::*;

// Pricing of deployments. Node that many options are not yet implemented buy serve as design of functionality.

/// A value that models the type of rating to be applied to a deployment.
/// A value denoting the strength of conviction of a vote.
#[derive(
	Encode,
	Decode,
	Copy,
	Clone,
	Eq,
	PartialEq,
	Ord,
	PartialOrd,
	RuntimeDebug,
	TypeInfo,
	MaxEncodedLen,
)]
pub enum RateType {
	/// Deployments is priced at 0. This can be useful in some cases, when infrastructure is owned and consumed
	/// from the same entity, bundled deployments (get x for free with your y), or running public good services.
	None,
	/// The deployment is priced at a fixed rate per single block
	FixedPerBlock,
	/// The deployment is priced per single block, the provider can update the rate at any time. Suitable for elastic deployments and trusted providers.
	VariablePerBlock,
	/// The deployment is priced per an arbitrary number of blocks at a fixed rate.
	FixedPerPeriod,
	/// The deployment is priced per an arbitrary number of blocks, the provider can update the rate at any time.
	VariablePerPeriod,
}

impl Default for RateType {
	fn default() -> Self {
		RateType::FixedPerBlock
	}
}

/// A rate for a given configuration is just a type of rate with an amount to charge.
#[derive(PartialEq, Eq, Clone, Encode, Decode, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub struct Rate<Balance> {
	pub rate_type: RateType,
	pub rate: Balance,
}

// TODO: this structure needs to be split by origin of the Rejection: Network or Provider
/// The reasons why a Subscription may have been rejected / not Awarded
#[derive(PartialEq, Eq, Clone, Encode, Decode, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub enum RejectionReason {
	/// Offerings for this Deployment Type could not be found
	NoOffering,
	/// No provider matching the selector could be found
	NoSelectableProvider,
	/// The network could not find capacity to allocate this subscription
	NoCapacity,
	/// The Provider reports that the Deployment Descriptor does not comply with the Deployment Type
	NonConformity,
	/// The awarded provider did not deploy in time.
	TimedOut,
}

/// Information about an Awarded Deployment

pub type AwardInfo<T> = (ProviderAccountIdOf<T>, Rate<BalanceOf<T>>);

/// A generic provider list,the pallet will make it "bound" to configured max.
pub type ProviderList<ProviderAccount> = Vec<ProviderAccount>;

/// An explicit provider selection
#[derive(PartialEq, Eq, Clone, Encode, Decode, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub enum ProviderListType {
	Select,
	Discard,
}

/// Subscribers can have multiple criteria for selecting providers that can be combined.
/// From explicit selection of a list of providers to the regional, networking properties.
/// There may be also network awarded qualifications.
#[derive(PartialEq, Eq, Clone, Encode, Decode, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub struct ProviderSelectorInfo<ProviderListOf> {
	/// Explicit selection of Providers
	pub explicit: Option<(ProviderListType, ProviderListOf)>,
	/// TODO: Selection by region.
	pub regional: Option<()>,
	/// TODO: Selection by network.
	pub network: Option<()>,
	/// TODO: Selection by qualification. I.e. on-chain certification of things such as being part of the 1KV programme, etc.
	pub qualification: Option<()>,
}

/// The Subscription and Provider pallets are loosely coupled. Generally speaking interactions
/// between both pallets are driven from Subscriptions. The following trait contains the exposed
/// interfaces.
pub trait Provider<T>
where
	T: Config,
	T: DinfraConfig,
{
	/// Ensure that a deployment_type exists
	fn contains_deployment_type_id(deployment_type_id: DeploymentTypeId) -> bool;
	fn award_deployment(
		deployment_type_id: DeploymentTypeId,
		deployment_descriptor: DeploymentDescriptorOf<T>,
		subscriber: SubscriberAccountIdOf<T>,
		provider_selection: ProviderSelectorInfo<ProviderList<ProviderAccountIdOf<T>>>,
	) -> Result<AwardInfo<T>, RejectionReason>;

	// let provider know about deployment termination
	fn terminate_deployment(
		deployment_type_id: DeploymentTypeId,
		provider: ProviderAccountIdOf<T>,
		subscriber: SubscriberAccountIdOf<T>,
	);
}
