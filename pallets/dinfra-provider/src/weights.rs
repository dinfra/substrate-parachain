
//! Autogenerated weights for `dinfra_provider`
//!
//! THIS FILE WAS AUTO-GENERATED USING THE SUBSTRATE BENCHMARK CLI VERSION 4.0.0-dev
//! DATE: 2023-11-20, STEPS: `20`, REPEAT: `100`, LOW RANGE: `[]`, HIGH RANGE: `[]`
//! WORST CASE MAP SIZE: `1000000`
//! HOSTNAME: `tuxraf`, CPU: `AMD Ryzen 7 4800H with Radeon Graphics`
//! EXECUTION: ``, WASM-EXECUTION: `Compiled`, CHAIN: `Some("dev")`, DB CACHE: 1024

// Executed Command:
// ./parachain/target/release/dinfra-node
// benchmark
// pallet
// --chain
// dev
// --pallet
// dinfra-provider
// --extrinsic
// *
// --steps
// 20
// --repeat
// 100
// --output
// parachain/pallets/dinfra-provider/src/weights.rs

#![cfg_attr(rustfmt, rustfmt_skip)]
#![allow(unused_parens)]
#![allow(unused_imports)]
#![allow(missing_docs)]

use frame_support::{traits::Get, weights::Weight};
use core::marker::PhantomData;

/// Weight functions for `dinfra_provider`.
pub struct WeightInfo<T>(PhantomData<T>);
impl<T: frame_system::Config> dinfra_provider::WeightInfo for WeightInfo<T> {
	/// Storage: `DinfraProvider::DeploymentTypes` (r:1 w:1)
	/// Proof: `DinfraProvider::DeploymentTypes` (`max_values`: None, `max_size`: Some(240), added: 2715, mode: `MaxEncodedLen`)
	/// Storage: `DinfraProvider::DeploymentTypesStatus` (r:0 w:1)
	/// Proof: `DinfraProvider::DeploymentTypesStatus` (`max_values`: None, `max_size`: Some(65), added: 2540, mode: `MaxEncodedLen`)
	/// The range of component `s` is `[1, 100]`.
	fn create_deployment_type_benchmark(_s: u32, ) -> Weight {
		// Proof Size summary in bytes:
		//  Measured:  `76`
		//  Estimated: `3705`
		// Minimum execution time: 41_626_000 picoseconds.
		Weight::from_parts(43_423_137, 0)
			.saturating_add(Weight::from_parts(0, 3705))
			.saturating_add(T::DbWeight::get().reads(1))
			.saturating_add(T::DbWeight::get().writes(2))
	}
}
