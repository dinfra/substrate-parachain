// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

#![cfg_attr(not(feature = "std"), no_std)]

pub use pallet::*;

#[cfg(test)]
mod mock;

#[cfg(test)]
mod tests;

#[cfg(feature = "runtime-benchmarks")]
mod benchmarking;

mod capacity;

mod offering;

/// The DINFRA Provider Pallet allows Infrastructure provider to express their capabilities on chain.
/// What can they deploy, what capacity they have and what rates do they charge.
/// All needs to be sufficiently specified so that subscribers can request deployments and the network
/// can allocate them to providers.

#[frame_support::pallet]
pub mod pallet {
	use frame_support::{
		dispatch::DispatchResultWithPostInfo, pallet_prelude::*, traits::ReservableCurrency,
	};
	use frame_system::pallet_prelude::*;
	use sp_std::vec::Vec;

	use super::{capacity::*, offering::*};
	use dinfra_common::{traits::*, *};

	/// A description type is basically a string limited by configuration
	pub type DescriptionOf<T> = BoundedVec<u8, <T as Config>::DescriptionEncodingLimit>;

	// TODO: Perhaps we should have global limits to deployment types and offerings.
	/// Configure the pallet by specifying the parameters and types on which it depends.
	/// This pallet stores CIDs subject to limits.
	#[pallet::config]
	pub trait Config: frame_system::Config + DinfraConfig {
		/// Because this pallet emits events, it depends on the runtime's definition of an event.
		type RuntimeEvent: From<Event<Self>> + IsType<<Self as frame_system::Config>::RuntimeEvent>;

		/// Deposit required to create Deployment Types or Offerings.
		#[pallet::constant]
		type ProviderDeposit: Get<BalanceOf<Self>>;

		/// The limit of the offering description
		type DescriptionEncodingLimit: Get<u32>;
	}

	#[pallet::pallet]
	pub struct Pallet<T>(_);

	/// The store for deployment types, it contains a readable Id and its Information.
	#[pallet::storage]
	#[pallet::getter(fn deployment_types)]
	pub type DeploymentTypes<T> = StorageMap<
		_,
		Twox64Concat,
		DeploymentTypeId,
		DeploymentTypeInfo<DeploymentTypeOf<T>, AccountIdOf<T>, DescriptionOf<T>>,
	>;

	/// The store for the status of the deployment types
	#[pallet::storage]
	#[pallet::getter(fn deployment_types_status)]
	pub type DeploymentTypesStatus<T> = StorageMap<
		_,
		Twox64Concat,
		DeploymentTypeId,
		DeploymentTypeStatus<AccountIdOf<T>, BalanceOf<T>>,
	>;

	/// The Offerings store. Here providers create their offers for concrete deployment types.
	/// They specify the pricing terms and capacity information so that subscriptions are created
	/// accordingly.
	///
	/// Normally subscrivers would search by DeploymentType but here they can also add a concrete
	/// provider in the search
	///
	#[pallet::storage]
	#[pallet::getter(fn offerings)]
	pub type Offerings<T> = StorageDoubleMap<
		_,
		Twox64Concat,
		DeploymentTypeId,
		Identity,
		ProviderAccountIdOf<T>,
		OfferingInfo<BalanceOf<T>>,
	>;

	/// Tracks the remaining capacity of each offering which are identified by DeploymentTypeId and
	/// Provider.
	#[pallet::storage]
	#[pallet::getter(fn offerings_load)]
	pub type OfferingsLoad<T> = StorageDoubleMap<
		_,
		Twox64Concat,
		DeploymentTypeId,
		Identity,
		ProviderAccountIdOf<T>,
		OfferingLoad,
	>;

	// TODO: this may grow a lot, it is one line per ongoing deployment
	// How many records it is actually feasible?
	/// Tracks the detail of current Deployments by offering
	#[pallet::storage]
	#[pallet::getter(fn offerings_deployments)]
	pub type OfferingsDeployments<T> = StorageNMap<
		_,
		(
			NMapKey<Twox64Concat, DeploymentTypeId>,
			NMapKey<Identity, ProviderAccountIdOf<T>>,
			NMapKey<Identity, SubscriberAccountIdOf<T>>,
		),
		DeploymentDescriptorOf<T>,
	>;

	// Pallets use events to inform users when important changes are made.
	// https://docs.substrate.io/v3/runtime/events-and-errors
	#[pallet::event]
	#[pallet::generate_deposit(pub(super) fn deposit_event)]
	pub enum Event<T: Config> {
		/// A deployment type was created. [id, CID, creator, deposit]
		DeploymentTypeCreated(DeploymentTypeId, DeploymentTypeOf<T>, T::AccountId, BalanceOf<T>),
		/// An offering was created. [deployment_type_id, provider, deposit]
		OfferingCreated(DeploymentTypeId, T::AccountId, BalanceOf<T>),
		/// An offering was updated.
		OfferingUpdated(DeploymentTypeId, T::AccountId),
		/// A Deployment was Awarded. [deployment_type_id,deployment_descriptor_cid, provider, subscriber]
		DeploymentAwarded(
			DeploymentTypeId,
			DeploymentDescriptorOf<T>,
			ProviderAccountIdOf<T>,
			SubscriberAccountIdOf<T>,
		),
	}

	// Errors inform users that something went wrong.
	#[pallet::error]
	pub enum Error<T> {
		/// CID is too long
		CIDTooLong,
		/// Description is too long
		DescriptionTooLong,
		/// The deployment type has already been created
		DeploymentTypeAlreadyExists,
		/// The ID of the Offered Deployment Type is already in use
		OfferedDeploymentTypeIdInUse,
		/// The Deployment Type ID does not exist
		DeploymentTypeDoesNotExist,
		/// The provider is already making a similar offering, for the same Deployment Type.
		OfferingAlreadyExists,
		/// The requested offering does not exist
		OfferingDoesNotExist,
	}

	#[pallet::hooks]
	impl<T: Config> Hooks<BlockNumberFor<T>> for Pallet<T> {}

	// TODO: Deposit can be returned, on request, when there is at least one offering, as an offering has its own deplosit.
	// TODO: The entry can be removed from storage when the last offering using it is removed, if the deposit was returned.
	// TODO: Could also be removed explicitly if it has not been instantiated.
	/// Create a Deployment Type, and capture the deposit.
	#[pallet::call]
	impl<T: Config> Pallet<T> {
		#[pallet::call_index(1)]
		#[pallet::weight(Weight::from_parts(10_000, 0) + T::DbWeight::get().reads_writes(1,2))]
		pub fn create_deployment_type(
			origin: OriginFor<T>,
			deployment_type_id: u32,
			deployment_type_cid: CID,
			description: Vec<u8>,
		) -> DispatchResultWithPostInfo {
			// Check signature
			let who = ensure_signed(origin)?;

			// Check bounds
			let bounded_dt: DeploymentTypeOf<T> =
				deployment_type_cid.try_into().map_err(|_| Error::<T>::CIDTooLong)?;

			let bounded_desc: DescriptionOf<T> =
				description.try_into().map_err(|_| Error::<T>::DescriptionTooLong)?;

			// Ensure is not already there
			ensure!(
				!DeploymentTypes::<T>::contains_key(deployment_type_id),
				Error::<T>::DeploymentTypeAlreadyExists
			);

			// Capture a deposit, for the storage used. The deposit is returned when the
			// Deployment type is used to create at least one offering that also requires another deposit
			let deposit = T::ProviderDeposit::get();
			T::Currency::reserve(&who, deposit)?;

			// Create the Deployment Type Status
			<DeploymentTypesStatus<T>>::insert(
				deployment_type_id,
				DeploymentTypeStatus::NotOffered { deposit: (who.clone(), deposit) },
			);

			// Insert the deployment type
			<DeploymentTypes<T>>::insert(
				deployment_type_id,
				DeploymentTypeInfo {
					deployment_type: bounded_dt.clone(),
					creator: who.clone(),
					description: bounded_desc,
				},
			);

			// Notify the creation of the deployment type
			Self::deposit_event(Event::DeploymentTypeCreated(
				deployment_type_id,
				bounded_dt,
				who,
				deposit,
			));

			// Return a successful DispatchResultWithPostInfo
			Ok(().into())
		}

		/// Allow Providers to create offerings for Deployment Types
		#[pallet::call_index(2)]
		#[pallet::weight(Weight::from_parts(10_000, 0) + T::DbWeight::get().reads_writes(1,1))]
		pub fn create_offering(
			origin: OriginFor<T>,
			deployment_type_id: DeploymentTypeId,
			capacity_type: CapacityType,
			capacity: CapacityUnit,
			rate_type: RateType,
			rate: BalanceOf<T>,
			state: OfferingState,
		) -> DispatchResultWithPostInfo {
			// Check signature
			let who = ensure_signed(origin)?;

			// Check that the Deployment type exists
			ensure!(
				DeploymentTypes::<T>::contains_key(deployment_type_id),
				Error::<T>::DeploymentTypeDoesNotExist
			);

			// Check if the offering already exists
			ensure!(
				!Offerings::<T>::contains_key(deployment_type_id, &who),
				Error::<T>::OfferingAlreadyExists
			);

			// Update the deployment usage
			let new_status = match DeploymentTypesStatus::<T>::get(deployment_type_id) {
				Some(DeploymentTypeStatus::NotOffered { deposit: d }) =>
					DeploymentTypeStatus::Offered { count: 1, deposit: d },
				Some(DeploymentTypeStatus::Offered { deposit: d, count: c }) =>
					DeploymentTypeStatus::Offered { count: c + 1, deposit: d },
				None => unreachable!("All deployment types have status"),
			};
			DeploymentTypesStatus::<T>::insert(deployment_type_id, new_status);

			// Capture the deposit for this offer
			// Capture a deposit, for the storage used. The deposit is returned when the
			// The offering is removed (must not be in use).
			let deposit = T::ProviderDeposit::get();
			T::Currency::reserve(&who, deposit)?;

			// Create the Offering
			<Offerings<T>>::insert(
				deployment_type_id,
				who.clone(),
				OfferingInfo {
					rate: Rate { rate_type, rate },
					capacity: Capacity { capacity_type, capacity },
					deposit,
				},
			);

			// Create the initial capacity for tracking
			<OfferingsLoad<T>>::insert(
				deployment_type_id,
				who.clone(),
				OfferingLoad {
					active_deployments: 0,
					remaining_capacity: Capacity { capacity_type, capacity },
					state,
				},
			);

			// Notify the creation of the offering
			Self::deposit_event(Event::OfferingCreated(deployment_type_id, who, deposit));

			// Return a successful DispatchResultWithPostInfo
			Ok(().into())
		}

		/// Allow modification of an offering capacity
		#[pallet::call_index(3)]
		#[pallet::weight(Weight::from_parts(10_000, 0) + T::DbWeight::get().reads_writes(1,1))]
		pub fn update_offering_capacity(
			origin: OriginFor<T>,
			deployment_type_id: DeploymentTypeId,
			capacity: CapacityUnit,
			state: OfferingState,
		) -> DispatchResultWithPostInfo {
			// Check signature
			let who = ensure_signed(origin)?;

			// Retrieve the Offering
			let offering_record = Offerings::<T>::get(deployment_type_id, &who);

			// Ensure the offering exists
			ensure!(offering_record.is_some(), Error::<T>::OfferingDoesNotExist);

			// Update the Offering with new capacity
			let mut offering = offering_record.unwrap();
			offering.capacity.capacity = capacity;

			<Offerings<T>>::insert(deployment_type_id, who.clone(), offering);

			// Update the current load
			let new_load = match OfferingsLoad::<T>::get(deployment_type_id, &who).unwrap() {
				OfferingLoad {
					active_deployments: d,
					remaining_capacity: Capacity { capacity_type: CapacityType::NumDeployments, .. },
					..
				} => OfferingLoad {
					active_deployments: d,
					remaining_capacity: Capacity {
						capacity_type: CapacityType::NumDeployments,
						capacity: capacity - d,
					},
					state,
				},
				OfferingLoad {
					active_deployments: d,
					remaining_capacity: Capacity { capacity_type: CapacityType::Infinite, .. },
					..
				} => OfferingLoad {
					active_deployments: d,
					remaining_capacity: Capacity {
						capacity_type: CapacityType::Infinite,
						capacity: 1,
					},
					state,
				},
			};

			<OfferingsLoad<T>>::insert(deployment_type_id, who.clone(), new_load);

			// Notify the creation of the offering
			Self::deposit_event(Event::OfferingUpdated(deployment_type_id, who));

			// Return a successful DispatchResultWithPostInfo
			Ok(().into())
		}
	}

	/// Helper functions
	impl<T: Config> Pallet<T> {}

	/// Functionality of the Provider that is exposed to the subscriber pallet
	impl<T: Config> Provider<T> for Pallet<T> {
		fn contains_deployment_type_id(deployment_type_id: DeploymentTypeId) -> bool {
			DeploymentTypes::<T>::contains_key(deployment_type_id)
		}

		fn terminate_deployment(
			deployment_type_id: DeploymentTypeId,
			provider: ProviderAccountIdOf<T>,
			subscriber: SubscriberAccountIdOf<T>,
		) {
			// Update the current load
			let new_load = match OfferingsLoad::<T>::get(deployment_type_id, &provider).unwrap() {
				OfferingLoad {
					active_deployments: d,
					remaining_capacity:
						Capacity { capacity_type: CapacityType::NumDeployments, capacity: c },
					state: s,
				} => OfferingLoad {
					active_deployments: d - 1,
					remaining_capacity: Capacity {
						capacity_type: CapacityType::NumDeployments,
						capacity: c + 1,
					},
					state: s,
				},
				OfferingLoad {
					active_deployments: d,
					remaining_capacity: Capacity { capacity_type: CapacityType::Infinite, .. },
					state: s,
				} => OfferingLoad {
					active_deployments: d - 1,
					remaining_capacity: Capacity {
						capacity_type: CapacityType::Infinite,
						capacity: 1,
					},
					state: s,
				},
			};

			OfferingsLoad::<T>::insert(deployment_type_id, &provider, &new_load);

			// Remove the awarded deployment from chain
			OfferingsDeployments::<T>::remove((deployment_type_id, &provider, &subscriber));
		}

		fn award_deployment(
			deployment_type_id: DeploymentTypeId,
			deployment_descriptor: DeploymentDescriptorOf<T>,
			subscriber: SubscriberAccountIdOf<T>,
			provider_selection: ProviderSelectorInfo<ProviderList<ProviderAccountIdOf<T>>>,
		) -> Result<(ProviderAccountIdOf<T>, Rate<BalanceOf<T>>), RejectionReason> {
			// TODO: This is a computationally complex procedure current implementation is MVP quality only.

			// track the selection process
			let (mut offerings, mut selected_providers, mut capable_providers) = (0, 0, 0);

			// TODO: This iterator may require limits.
			let mut available_providers: Vec<(
				ProviderAccountIdOf<T>,
				OfferingInfo<BalanceOf<T>>,
				OfferingLoad,
			)> = Offerings::<T>::iter_prefix(&deployment_type_id)
				// Track offerings
				.filter(|(..)| {
					offerings += 1;
					true
				})
				// Filter by Provider that meet the subscriptor Selector
				.filter(|(provider, _offering)| match (&provider, &provider_selection) {
					// provider explicitly selected
					(
						p,
						ProviderSelectorInfo {
							explicit: Some((ProviderListType::Select, list)),
							..
						},
					) if list.contains(p) => true,
					// provider explicitly discarded
					(
						p,
						ProviderSelectorInfo {
							explicit: Some((ProviderListType::Discard, list)),
							..
						},
					) if !list.contains(p) => true,
					// TODO: Other selection parameters
					// in other case reject
					(_, _) => false,
				})
				// Track selected providers
				.filter(|(..)| {
					selected_providers += 1;
					true
				})
				// Add the Remaining Capacity
				.map(|(provider, offering)| {
					(
						provider.clone(),
						offering,
						OfferingsLoad::<T>::get(&deployment_type_id, &provider).unwrap(),
					)
				})
				// Filter by available Capacity
				.filter(|(_provider, _offering, load)| match load {
					// provider has closed the offering
					OfferingLoad { state: OfferingState::Closed, .. } => false,
					// The capacity of the offering has no limit
					OfferingLoad {
						remaining_capacity: Capacity { capacity_type: CapacityType::Infinite, .. },
						..
					} => true,
					// Capacity is measured in number of deployments, and there is capacity left
					OfferingLoad {
						remaining_capacity:
							Capacity { capacity_type: CapacityType::NumDeployments, capacity: n },
						..
					} if n > &0 => true,
					// TODO: other capacity metrics can come here
					_ => false,
				})
				// Track capable providers
				.filter(|(..)| {
					capable_providers += 1;
					true
				})
				// Now Collect the available ones
				.collect();

			// Now lets see how the selection went, at which stage the pipeline got dry:
			match (offerings, selected_providers, capable_providers) {
				(0, ..) => Err(RejectionReason::NoOffering),
				(_, 0, _) => Err(RejectionReason::NoSelectableProvider),
				(_, _, 0) => Err(RejectionReason::NoCapacity),
				_ => Ok(()),
			}?;

			// Lets assign to the least loaded provider
			// We dont use only a minimum search on active_deployments as many could come with same value (0) and
			// perhaps the implementation is not deterministic, so we soft instead adding the providerId as second key
			// then there is always one unique right choice.

			available_providers.sort_by(|(provider1, _, load1), (provider2, _, load2)| {
				load1
					.active_deployments
					.cmp(&load2.active_deployments)
					.then_with(|| provider1.cmp(&provider2))
			});

			// And we have an award winner!
			let (awarded_provider, awarded_offering, awarded_load) =
				available_providers.first().unwrap();

			// Write the deployment award on chain
			OfferingsDeployments::<T>::insert(
				(deployment_type_id, awarded_provider, &subscriber),
				&deployment_descriptor,
			);

			// Update the provider's Load
			let new_load = match awarded_load {
				// Infinite capacity, we just note one additional active deployment
				OfferingLoad {
					active_deployments: d,
					remaining_capacity: Capacity { capacity_type: CapacityType::Infinite, .. },
					state: s,
				} => OfferingLoad {
					active_deployments: d + 1,
					remaining_capacity: Capacity {
						capacity_type: CapacityType::Infinite,
						capacity: 0,
					},
					state: s.clone(),
				},
				// Limited capacity, we add one deployment and remove one from capacity
				OfferingLoad {
					active_deployments: d,
					remaining_capacity:
						Capacity { capacity_type: CapacityType::NumDeployments, capacity: c },
					state: s,
				} => OfferingLoad {
					active_deployments: d + 1,
					remaining_capacity: Capacity {
						capacity_type: CapacityType::NumDeployments,
						capacity: c - 1,
					},
					state: s.clone(),
				},
				// TODO: other capacity tracking types would come here.
			};

			// Update the load tracking
			<OfferingsLoad<T>>::insert(deployment_type_id, awarded_provider, new_load);

			// Notify the award
			Self::deposit_event(Event::DeploymentAwarded(
				deployment_type_id,
				deployment_descriptor,
				awarded_provider.clone(),
				subscriber,
			));

			// Return the Awarding Result
			Ok((awarded_provider.clone(), awarded_offering.rate.clone()))
		}
	}
}
