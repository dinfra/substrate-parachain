use codec::{Decode, Encode};
use frame_support::{assert_noop, assert_ok};

use dinfra_common::{traits::*, BalanceOf};

use crate::{
	capacity::{Capacity, CapacityType, OfferingLoad},
	mock::*,
	offering::{OfferingInfo, OfferingState},
	Error,
};

#[test]
fn can_store_a_deployment_type() {
	new_test_ext().execute_with(|| {
		// Set the deployment type for the pallet
		assert_ok!(ProviderModule::create_deployment_type(
			RuntimeOrigin::signed(1),
			1,
			b"1234567890".to_vec(),
			b"0987654321".to_vec()
		));

		// find the stored deployment information
		let dti = ProviderModule::deployment_types(1).unwrap();

		assert_eq!(dti.creator, 1);
		assert_eq!(dti.deployment_type, b"1234567890".to_vec());
		assert_eq!(dti.description, b"0987654321".to_vec());
	});
}

#[test]
fn can_not_store_a_too_long_deployment_type() {
	new_test_ext().execute_with(|| {
		// Set a deployment type longer than the configured limit, must fail
		assert_noop!(
			ProviderModule::create_deployment_type(
				RuntimeOrigin::signed(1),
				1,
				b"12345678901234567890".to_vec(),
				b"0987654321".to_vec()
			),
			Error::<Test>::CIDTooLong
		);
	});
}

#[test]
fn can_not_store_a_too_long_description() {
	new_test_ext().execute_with(|| {
		// Set a description longer than the configured limit, must fail
		assert_noop!(
			ProviderModule::create_deployment_type(
				RuntimeOrigin::signed(1),
				1,
				b"1234567890".to_vec(),
				b"0987654321098765432109876543210987654321".to_vec()
			),
			Error::<Test>::DescriptionTooLong
		);
	});
}

#[test]
fn can_not_store_same_deployment_descriptor_twice() {
	new_test_ext().execute_with(|| {
		assert_ok!(ProviderModule::create_deployment_type(
			RuntimeOrigin::signed(1),
			1,
			b"1234567890".to_vec(),
			b"09876543210".to_vec()
		));
		// Attempt to store a second time, same CID
		assert_noop!(
			ProviderModule::create_deployment_type(
				RuntimeOrigin::signed(1),
				1,
				b"1234567890".to_vec(),
				b"09876543210".to_vec()
			),
			Error::<Test>::DeploymentTypeAlreadyExists
		);
	});
}

// check serialization works before using as storage value.
#[test]
fn can_encode_an_offering() {
	new_test_ext().execute_with(|| {
		let o = OfferingInfo::<BalanceOf<Test>> {
			rate: Rate { rate_type: RateType::None, rate: 0 },
			capacity: Capacity { capacity_type: CapacityType::Infinite, capacity: 0 },
			deposit: 10,
		};

		let encoded = o.encode();
		let decoded = OfferingInfo::decode(&mut &encoded[..]).unwrap();
		assert_eq!(o, decoded);
	});
}

#[test]
fn can_create_an_offering() {
	new_test_ext().execute_with(|| {
		// Create a Deployment Type
		assert_ok!(ProviderModule::create_deployment_type(
			RuntimeOrigin::signed(1),
			1,
			b"1234567890".to_vec(),
			b"0987654321".to_vec()
		));

		// Create an Offering for the Deployment Type
		assert_ok!(ProviderModule::create_offering(
			RuntimeOrigin::signed(1),
			1,
			CapacityType::NumDeployments,
			20,
			RateType::FixedPerBlock,
			1,
			OfferingState::Open,
		));
	});
}

#[test]
fn can_not_create_a_offering_with_invalid_deployment_type() {
	new_test_ext().execute_with(|| {
		// Create an Offering for the Deployment Type
		assert_noop!(
			ProviderModule::create_offering(
				RuntimeOrigin::signed(1),
				10,
				CapacityType::NumDeployments,
				20,
				RateType::FixedPerBlock,
				1,
				OfferingState::Open,
			),
			Error::<Test>::DeploymentTypeDoesNotExist
		);
	});
}

#[test]
fn can_not_create_a_offering_twice() {
	new_test_ext().execute_with(|| {
		// Create a Deployment Type
		assert_ok!(ProviderModule::create_deployment_type(
			RuntimeOrigin::signed(1),
			1,
			b"1234567890".to_vec(),
			b"0987654321".to_vec()
		));

		// Create an Offering for the Deployment Type
		assert_ok!(ProviderModule::create_offering(
			RuntimeOrigin::signed(1),
			1,
			CapacityType::NumDeployments,
			2,
			RateType::FixedPerBlock,
			1,
			OfferingState::Open,
		));

		// Create an Offering for the Deployment Type
		assert_noop!(
			ProviderModule::create_offering(
				RuntimeOrigin::signed(1),
				1,
				CapacityType::NumDeployments,
				2,
				RateType::FixedPerBlock,
				1,
				OfferingState::Open,
			),
			Error::<Test>::OfferingAlreadyExists
		);
	});
}

#[test]
fn deployments_are_awareded_as_load_balancing() {
	new_test_ext().execute_with(|| {
		// Create a Deployment Type 1
		assert_ok!(ProviderModule::create_deployment_type(
			RuntimeOrigin::signed(1),
			1,
			b"1234567890".to_vec(),
			b"0987654321".to_vec()
		));

		// Create an Offering for the Deployment Type 1 by provider 1 (signatory)
		assert_ok!(ProviderModule::create_offering(
			RuntimeOrigin::signed(1),
			1,
			CapacityType::NumDeployments,
			2,
			RateType::FixedPerBlock,
			1,
			OfferingState::Open,
		));

		// Create an Offering for the Deployment Type 1 by provider 2 (signatory)
		assert_ok!(ProviderModule::create_offering(
			RuntimeOrigin::signed(2),
			1,
			CapacityType::NumDeployments,
			2,
			RateType::FixedPerBlock,
			2,
			OfferingState::Open,
		));

		// Simulate an award request from subscriber 3, regarding deployment type 1
		assert_ok!(ProviderModule::award_deployment(
			1,
			b"abcdf123".to_vec().try_into().unwrap(),
			3,
			ProviderSelectorInfo {
				explicit: Some((ProviderListType::Select, vec![1, 2])),
				regional: None,
				network: None,
				qualification: None,
			}
		));

		// Provider 1 should have been awarded this deployment
		assert!(match ProviderModule::offerings_load(1, 1).unwrap() {
			OfferingLoad { active_deployments: 1, .. } => true,
			_ => false,
		});

		// Simulate an award request from subscriber 4, regarding deployment type 1
		assert_ok!(ProviderModule::award_deployment(
			1,
			b"abcdf123".to_vec().try_into().unwrap(),
			4,
			ProviderSelectorInfo {
				explicit: Some((ProviderListType::Select, vec![1, 2])),
				regional: None,
				network: None,
				qualification: None,
			}
		));

		// Provider 2 should have been awarded this deployment
		assert!(match ProviderModule::offerings_load(1, 2).unwrap() {
			OfferingLoad { active_deployments: 1, .. } => true,
			_ => false,
		});

		// Simulate an award request from subscriber 5, regarding deployment type 1
		// but has banned the only 2 providers available
		assert_noop!(
			ProviderModule::award_deployment(
				1,
				b"abcdf123".to_vec().try_into().unwrap(),
				5,
				ProviderSelectorInfo {
					explicit: Some((ProviderListType::Discard, vec![1, 2])),
					regional: None,
					network: None,
					qualification: None,
				}
			),
			RejectionReason::NoSelectableProvider
		);

		// Simulate an award request from subscriber 6, regarding deployment type 1
		// A discard list that allows providers 1,2
		assert_ok!(ProviderModule::award_deployment(
			1,
			b"abcdf123".to_vec().try_into().unwrap(),
			6,
			ProviderSelectorInfo {
				explicit: Some((ProviderListType::Discard, vec![10, 20])),
				regional: None,
				network: None,
				qualification: None,
			}
		));

		// Provider 1 should have been awarded this deployment, but it is now out of capacity
		assert!(match ProviderModule::offerings_load(1, 1).unwrap() {
			OfferingLoad {
				active_deployments: 2,
				remaining_capacity: Capacity { capacity: 0, .. },
				..
			} => true,
			_ => false,
		});

		// Simulate an award request from subscriber 7, regarding deployment type 1
		// A discard list that allows providers 1,2
		assert_ok!(ProviderModule::award_deployment(
			1,
			b"abcdf123".to_vec().try_into().unwrap(),
			7,
			ProviderSelectorInfo {
				explicit: Some((ProviderListType::Discard, vec![10, 20])),
				regional: None,
				network: None,
				qualification: None,
			}
		));

		// Provider 2 should have been awarded this deployment, but it is now out of capacity
		let _d = ProviderModule::offerings_load(1, 2).unwrap();
		assert!(match ProviderModule::offerings_load(1, 2).unwrap() {
			OfferingLoad {
				active_deployments: 2,
				remaining_capacity: Capacity { capacity: 0, .. },
				..
			} => true,
			_ => false,
		});

		// Now there is no capacity for this deployment type at all
		// an award should just fail

		assert_noop!(
			ProviderModule::award_deployment(
				1,
				b"abcdf123".to_vec().try_into().unwrap(),
				8,
				ProviderSelectorInfo {
					explicit: Some((ProviderListType::Select, vec![1, 2])),
					regional: None,
					network: None,
					qualification: None,
				}
			),
			RejectionReason::NoCapacity
		);
	});
}

#[test]
fn can_not_award_an_unexistent_deployment() {
	new_test_ext().execute_with(|| {
		// Create a Deployment Type 1
		assert_ok!(ProviderModule::create_deployment_type(
			RuntimeOrigin::signed(1),
			1,
			b"1234567890".to_vec(),
			b"0987654321".to_vec()
		));

		// Create an Offering for the Deployment Type 1 by provider 1 (signatory)
		assert_ok!(ProviderModule::create_offering(
			RuntimeOrigin::signed(1),
			1,
			CapacityType::NumDeployments,
			2,
			RateType::FixedPerBlock,
			1,
			OfferingState::Open,
		));

		// Request deployment type 2, which does not exists
		// Will return that there is no provider offering this
		assert_noop!(
			ProviderModule::award_deployment(
				1,
				b"abcdf123".to_vec().try_into().unwrap(),
				2,
				ProviderSelectorInfo {
					explicit: Some((ProviderListType::Discard, vec![1, 2])),
					regional: None,
					network: None,
					qualification: None,
				}
			),
			RejectionReason::NoSelectableProvider
		);
	});
}

#[test]
fn can_not_award_an_existent_deployment_no_one_offers() {
	new_test_ext().execute_with(|| {
		// Create a Deployment Type 1
		assert_ok!(ProviderModule::create_deployment_type(
			RuntimeOrigin::signed(1),
			1,
			b"1234567890".to_vec(),
			b"0987654321".to_vec()
		));

		// Request deployment type 2, which does not exists
		// Will return that there is no provider offering this
		assert_noop!(
			ProviderModule::award_deployment(
				1,
				b"abcdf123".to_vec().try_into().unwrap(),
				2,
				ProviderSelectorInfo {
					explicit: Some((ProviderListType::Discard, vec![1, 2])),
					regional: None,
					network: None,
					qualification: None,
				}
			),
			RejectionReason::NoOffering
		);
	});
}

#[test]
fn can_not_award_to_closed_offering() {
	new_test_ext().execute_with(|| {
		// Create a Deployment Type 1
		assert_ok!(ProviderModule::create_deployment_type(
			RuntimeOrigin::signed(1),
			1,
			b"1234567890".to_vec(),
			b"0987654321".to_vec()
		));

		// Create an Offering for the Deployment Type 1 by provider 1 (signatory)
		assert_ok!(ProviderModule::create_offering(
			RuntimeOrigin::signed(1),
			1,
			CapacityType::NumDeployments,
			2,
			RateType::FixedPerBlock,
			1,
			OfferingState::Open,
		));

		// Simulate an award request from subscriber 2, regarding deployment type 1
		assert_ok!(ProviderModule::award_deployment(
			1,
			b"abcdf123".to_vec().try_into().unwrap(),
			3,
			ProviderSelectorInfo {
				explicit: Some((ProviderListType::Select, vec![1, 2])),
				regional: None,
				network: None,
				qualification: None,
			}
		));

		// Now there is one deployment remaining of capacity, but the provider closes the offering

		assert_ok!(ProviderModule::update_offering_capacity(
			RuntimeOrigin::signed(1),
			1,
			2,
			OfferingState::Closed,
		));

		// Next award should fail per State, with No Capacity
		assert_noop!(
			ProviderModule::award_deployment(
				1,
				b"abcdf123".to_vec().try_into().unwrap(),
				4,
				ProviderSelectorInfo {
					explicit: Some((ProviderListType::Select, vec![1, 2])),
					regional: None,
					network: None,
					qualification: None,
				}
			),
			RejectionReason::NoCapacity
		);
	});
}
