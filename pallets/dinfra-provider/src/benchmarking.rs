//! Benchmarking setup for pallet-dinfra-provider

use crate::*;

use crate::Config;
#[allow(unused)]
use crate::Pallet as Provider;
use frame_benchmarking::{benchmarks, impl_benchmark_test_suite, whitelisted_caller};
use frame_support::{pallet_prelude::*, traits::Currency};
use frame_system::RawOrigin;

benchmarks! {
 create_deployment_type_benchmark {
   // Benchmark setup phase
   let s in 1 .. 100;
   let caller: T::AccountId = whitelisted_caller();

	// Fund the caller account, for convenience use a minimum deposit multiplier
	T::Currency::make_free_balance_be(&caller, T::ProviderDeposit::get() * 100u32.into());

 }: create_deployment_type(RawOrigin::Signed(caller), s, b"1234567890".to_vec(), b"1234567890".to_vec()) // Execution phase
 verify {
	   // The object was created
   assert!(DeploymentTypes::<T>::get(s).is_some())
 }
}

impl_benchmark_test_suite!(Provider, crate::mock::new_test_ext(), crate::mock::Test,);
