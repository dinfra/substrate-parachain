// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

use codec::{Decode, Encode, MaxEncodedLen};
use scale_info::TypeInfo;
use sp_runtime::RuntimeDebug;

use crate::capacity::Capacity;
use dinfra_common::traits::*;

/// Type for tracking how many offers there are of a given deployment type
pub type DeploymentTypeUsage = u32;

/// Deployment type additional information.
/// This additional data is meant to facilitate identifying the CID and not just using
/// a CID, a description and who created it.
#[derive(PartialEq, Eq, Clone, Encode, Decode, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub struct DeploymentTypeInfo<DeploymentType, AccountId, Description> {
	pub deployment_type: DeploymentType,
	pub creator: AccountId,
	pub description: Description,
}

/// A type to note whether a deployment type is in used or not, once in use (offered)
/// It is considered owned by the system and the deposit can be returned.
#[derive(Clone, Eq, PartialEq, Encode, Decode, TypeInfo, MaxEncodedLen, RuntimeDebug)]
pub enum DeploymentTypeStatus<AccountId, Balance> {
	/// The deployment type is not in use, the deposit belongs to the creator
	/// and will be released when the deployment is used to create an offer, or
	/// when the creator deletes it.
	NotOffered { deposit: (AccountId, Balance) },
	/// The deployment type is in use, the deposit has been returned.
	/// The number of usages is also tracked an when it becomes 0 it can be removed.
	Offered { deposit: (AccountId, Balance), count: DeploymentTypeUsage },
}

/// It may be possible to close an Offering if, for example, a new version of the offering
///
#[derive(PartialEq, Eq, Clone, Encode, Decode, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub enum OfferingState {
	/// The offering is open for deployments
	Open,
	/// The offering is not accepting more deployments
	Closed,
}

/// An offering is something that an Infrastructure provider offers for consumption by an
/// Infrastructure consumer. It requires to be specified, so that the consumer knows how to order
/// how much it will cost, so that the provider gets the required information for deployment and
/// also for the network to know if deployment requests can be assigned to this provider or not.
/// In general an offering is made by an Account, the provider, and includes the Deployment Type,
/// Capacity and Pricing information.

#[derive(PartialEq, Eq, Clone, Encode, Decode, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub struct OfferingInfo<Balance> {
	/// The specification if the rate that applies to this offering
	pub rate: Rate<Balance>,
	/// The capacity of the provider
	pub capacity: Capacity,
	/// The deposit captured for this Offering, the provider paid.
	pub deposit: Balance,
}
