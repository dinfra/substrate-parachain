// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

use crate::offering::OfferingState;
use codec::{Decode, Encode, MaxEncodedLen};
use scale_info::TypeInfo;
use sp_runtime::RuntimeDebug;

pub type CapacityUnit = u32;

// TODO: Other capacity models may be possible, however this is not simple to address in general.
// Infrastructure may have different bottlenecks, and deployment can be parametric.
// It is common practice to arrange infrastructure by configuration making capacity predictable and
// NumDeployments cover that base case.

/// A Value that models the capacity of a provider measured with different strategies.
#[derive(
	Encode,
	Decode,
	Copy,
	Clone,
	Eq,
	PartialEq,
	Ord,
	PartialOrd,
	RuntimeDebug,
	TypeInfo,
	MaxEncodedLen,
)]
pub enum CapacityType {
	/// Infinite Capacity or not measured. Some deployments may represent on demand tasks for which
	/// capacity modelling is not required.
	Infinite,
	/// Capacity is measured in Number of Deployments
	NumDeployments,
}

impl Default for CapacityType {
	fn default() -> Self {
		CapacityType::NumDeployments
	}
}

// TODO: other forms of capacity are possible. Parametric capacity would be interesting,
// A generic counter and perhaps the provider itself declaring how much capcity was reduced after a
// deployment was enacted.
/// Capacity is ensured by type and counter
#[derive(PartialEq, Eq, Clone, Encode, Decode, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub struct Capacity {
	pub capacity_type: CapacityType,
	pub capacity: CapacityUnit,
}

/// A type to track the number of current deployed descriptors
pub type ActiveDeployments = u32;

/// A type to track current load and capacity
#[derive(PartialEq, Eq, Clone, Encode, Decode, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub struct OfferingLoad {
	/// The number of deployments currently served
	pub active_deployments: ActiveDeployments,
	/// The capacity still remaining
	pub remaining_capacity: Capacity,
	/// Whether the provider has closed the offering manually.
	pub state: OfferingState,
}
