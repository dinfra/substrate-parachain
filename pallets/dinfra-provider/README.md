# DINFRA provider pallet

A provider is an account that can provide infrastructure for others to subscribe.

## Concepts

- Deployment Type: Types of deployments are specified by JSON Schemas, and identified by IPFS CIDs of the schema.
- Rates: Pricing establishes how the Deployment is going to be charged, there can be several pricing models. The simple form is fixed cost per block or free.
- Capacity: Capacity models the capability of a provider in terms of numbers of deployments that can be provided. There can be several capacity models. Number of deployments is a simple form of capacity.
- Offering: Is a tuple made do: Deployment Type, Pricing and Capacity.

Providers provide deployments in form of offerings to subscribers.

The pallet is self documented, and most functionalities are unit-tested, check both for details.
