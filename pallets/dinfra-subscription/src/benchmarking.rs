//! Benchmarking setup for pallet-dinfra-provider

use crate::{selector::ProviderListType, *};

#[allow(unused)]
use crate::Pallet as Subscription;
use frame_benchmarking::{benchmarks, impl_benchmark_test_suite, whitelisted_caller};
use frame_support::{pallet_prelude::*, traits::Currency};
use frame_system::RawOrigin;

benchmarks! {
	create_provider_selector_benchmark {
		let s in 0 .. 100;
		let caller: T::AccountId = whitelisted_caller();
		let provider: T::AccountId = whitelisted_caller();
		// Fund the caller account, for convenience use a minimum deposit multiplier
		T::Currency::make_free_balance_be(&caller, T::SubscriptionDeposit::get() * 100u32.into());
	}: create_provider_selector(
		RawOrigin::Signed(caller),
		s,
		ProviderListType::Select,
		[provider].to_vec()
	)
	verify {
		assert!(ProviderSelectors::<T>::get(s).is_some());
	}
}

impl_benchmark_test_suite!(Subscription, crate::mock::new_test_ext(), crate::mock::Test,);
