use codec::{Decode, Encode};
use frame_support::{assert_noop, assert_ok, traits::Get};

use crate::{
	mock::*,
	selector::ProviderSelectorStatus,
	subscription::{
		AwardPreference, SubscriptionFunding, SubscriptionInfo, SubscriptionState,
		TerminationReason,
	},
	AwardPreferenceOf, CallOf, Error, ProviderListOf, SubscriptionInfoOf, SUBSCRIPTION_ID,
};
use dinfra_common::traits::*;
use frame_support::traits::schedule::DispatchTime;

#[test]
fn can_serialize_provider_selector() {
	new_test_ext().execute_with(|| {
		// Create alist of providers we like
		let list: ProviderListOf<Test> = [1, 2, 3].to_vec().try_into().unwrap();

		// Create a selection for those.
		let s = ProviderSelectorInfo::<ProviderListOf<Test>> {
			explicit: Some((ProviderListType::Select, list)),
			regional: None,
			network: None,
			qualification: None,
		};
		let encoded = s.encode();
		let decoded = ProviderSelectorInfo::decode(&mut &encoded[..]).unwrap();
		assert_eq!(s, decoded);
	});
}

#[test]
fn can_create_a_provider_selector() {
	new_test_ext().execute_with(|| {
		assert_ok!(SubscriptionModule::create_provider_selector(
			RuntimeOrigin::signed(1),
			1,
			ProviderListType::Select,
			[1, 2, 3].to_vec()
		));

		// find the provider selector and its usage
		let selector = SubscriptionModule::provider_selectors(1).unwrap();
		let usage = SubscriptionModule::provider_selectors_status(1).unwrap();

		// match data structure and owner
		match (selector, usage) {
			(
				ProviderSelectorInfo { explicit: Some((ProviderListType::Select, ..)), .. },
				ProviderSelectorStatus::NotUsed { deposit: (who, ..) },
			) => assert_eq!(who, 1),
			_ => panic!("Expected match!"),
		}
	});
}

#[test]
fn can_not_create_a_provider_selector_with_too_many_selections() {
	new_test_ext().execute_with(|| {
		assert_noop!(
			SubscriptionModule::create_provider_selector(
				RuntimeOrigin::signed(1),
				1,
				ProviderListType::Select,
				[1, 2, 3, 4, 5].to_vec()
			),
			Error::<Test>::ProviderListTooLong
		);
	});
}

#[test]
fn can_serialize_a_subscription_info() {
	new_test_ext().execute_with(|| {
		let si = SubscriptionInfoOf::<Test> {
			deployment_descriptor: vec![1, 2, 3].try_into().unwrap(),
			deployment_type: 1,
			award_preference: AwardPreferenceOf::<Test>::Explicit { provider: 1 },
		};

		let encoded = si.encode();
		let decoded = SubscriptionInfoOf::<Test>::decode(&mut &encoded[..]).unwrap();
		assert_eq!(si, decoded);
	})
}

#[test]
fn can_create_a_subscription_with_provider() {
	new_test_ext().execute_with(|| {
		assert_ok!(SubscriptionModule::create_subscription(
			RuntimeOrigin::signed(101),
			b"1234567890".to_vec(),
			1,
			None,
			Some(1),
			10
		));

		let sub = SubscriptionModule::subscriptions(101).unwrap();

		match sub {
			SubscriptionInfo {
				award_preference: AwardPreference::Explicit { provider: p },
				..
			} => assert_eq!(p, 1),
			_ => panic!("Expected Match!"),
		};
	})
}

#[test]
fn can_not_create_a_subscription_with_provider_but_missing_deployment_type() {
	new_test_ext().execute_with(|| {
		assert_noop!(
			SubscriptionModule::create_subscription(
				RuntimeOrigin::signed(101),
				b"1234567890".to_vec(),
				21,
				None,
				Some(1),
				10
			),
			Error::<Test>::InvalidDeploymentTypeId
		);
	});
}

#[test]
fn can_create_a_subscription_with_selector() {
	new_test_ext().execute_with(|| {
		// We create a sector for 3 providers
		assert_ok!(SubscriptionModule::create_provider_selector(
			RuntimeOrigin::signed(101),
			1,
			ProviderListType::Select,
			[1, 2, 3].to_vec()
		));

		// we create the subscription using the selector
		assert_ok!(SubscriptionModule::create_subscription(
			RuntimeOrigin::signed(101),
			b"1234567890".to_vec(),
			1,
			Some(1),
			None,
			10
		));

		let sub = SubscriptionModule::subscriptions(101).unwrap();

		match sub {
			SubscriptionInfo {
				award_preference: AwardPreference::Selection { selector: s },
				..
			} => assert_eq!(s, 1),
			_ => panic!("Expected Match!"),
		};
	})
}

#[test]
fn can_not_create_a_subscription_with_provider_and_selector() {
	new_test_ext().execute_with(|| {
		assert_noop!(
			SubscriptionModule::create_subscription(
				RuntimeOrigin::signed(101),
				b"1234567890".to_vec(),
				1,
				Some(1),
				Some(1),
				10
			),
			Error::<Test>::SelectorOrProviderRequired
		);
	})
}

#[test]
fn can_not_create_a_subscription_with_little_funding() {
	new_test_ext().execute_with(|| {
		assert_noop!(
			SubscriptionModule::create_subscription(
				RuntimeOrigin::signed(101),
				b"1234567890".to_vec(),
				1,
				None,
				Some(1),
				2
			),
			Error::<Test>::NotEnoughFunding
		);
	})
}

#[test]
fn can_schedule_the_termination_of_a_subscription() {
	// We just want to test that the scheduler pallet is properly integrated and working,
	new_test_ext().execute_with(|| {
		// We create a subscription for which schedule a termination, with owner/id 1

		assert_ok!(SubscriptionModule::create_subscription(
			RuntimeOrigin::signed(101),
			b"1234567890".to_vec(),
			1,
			None,
			Some(1),
			10
		));

		SubscriptionModule::do_schedule(
			(SUBSCRIPTION_ID, "test", 101).using_encoded(sp_io::hashing::blake2_256),
			DispatchTime::After(3u8.into()),
			CallOf::<Test>::from(crate::Call::terminate_subscription { subscription_owner: 101 }),
		);

		// Advance to block 3 and check that the subscription is in Created state
		run_to(3);
		assert!(match SubscriptionModule::subscriptions_status(101) {
			Some(SubscriptionState::Created) => true,
			_ => false,
		});
		// Advance to block 4 and check that the subscription was cancelled by the network
		run_to(4);
		assert!(match SubscriptionModule::subscriptions_status(101) {
			Some(SubscriptionState::Cancelled {
				reason: TerminationReason::TerminatedByNetwork,
			}) => true,
			_ => false,
		});
	});
}

// If a subscriber tops up a subscription prior to termination we need to push forward the termination moment.
#[test]
fn can_reschedule_the_termination_of_a_subscription() {
	// We just want to test that the scheduler pallet is properly integrated and working,
	new_test_ext().execute_with(|| {
		// We create a subscription for which schedule a termination, with owner/id 1

		assert_ok!(SubscriptionModule::create_subscription(
			RuntimeOrigin::signed(101),
			b"1234567890".to_vec(),
			1,
			None,
			Some(1),
			10
		));

		SubscriptionModule::do_schedule(
			(SUBSCRIPTION_ID, "test", 101).using_encoded(sp_io::hashing::blake2_256),
			DispatchTime::At(4u8.into()),
			CallOf::<Test>::from(crate::Call::terminate_subscription { subscription_owner: 101 }),
		);

		// Advance to block 3 and check that the subscription is in Created state
		run_to(2);
		assert!(match SubscriptionModule::subscriptions_status(101) {
			Some(SubscriptionState::Created) => true,
			_ => false,
		});

		//re-schedule it in block 3: reschedule for block 6
		run_to(3);

		SubscriptionModule::do_reschedule(
			(SUBSCRIPTION_ID, "test", 101).using_encoded(sp_io::hashing::blake2_256),
			DispatchTime::At(5u32.into()),
		);

		// Advance to block 4 and the subscription was not yet cancelled
		assert!(match SubscriptionModule::subscriptions_status(101) {
			Some(SubscriptionState::Created) => true,
			_ => false,
		});

		// Finally at block 6 was cancelled by network
		run_to(5);
		assert!(match SubscriptionModule::subscriptions_status(101) {
			Some(SubscriptionState::Cancelled {
				reason: TerminationReason::TerminatedByNetwork,
			}) => true,
			_ => false,
		});
	});
}

// Will just check that the subscription funding is working as expected
#[test]
fn check_subscription_funding_arithmetic() {
	new_test_ext().execute_with(|| {
		// simple whole calculation
		let s = SubscriptionFunding {
			deployed_at: Some(1),
			paid_until: Some(1),
			remaining_funds: 5,
			rate: Rate { rate_type: RateType::FixedPerBlock, rate: 1 },
		};
		assert_eq!(
			SubscriptionModule::expected_termination(s),
			Some(DispatchTime::At(6u32.into()))
		);

		// has not started yet
		let s = SubscriptionFunding {
			deployed_at: None,
			paid_until: None,
			remaining_funds: 5,
			rate: Rate { rate_type: RateType::FixedPerBlock, rate: 1 },
		};
		assert_eq!(SubscriptionModule::expected_termination(s), None);

		// has a 0 rate
		let s = SubscriptionFunding {
			deployed_at: Some(1),
			paid_until: Some(1),
			remaining_funds: 5,
			rate: Rate { rate_type: RateType::FixedPerBlock, rate: 0 },
		};
		assert_eq!(SubscriptionModule::expected_termination(s), None);

		// There will be remaining funds...
		let s = SubscriptionFunding {
			deployed_at: Some(1),
			paid_until: Some(1),
			remaining_funds: 20,
			rate: Rate { rate_type: RateType::FixedPerBlock, rate: 3 },
		};
		assert_eq!(SubscriptionModule::expected_termination(s), Some(DispatchTime::At(7)));
	});
}

// Lifecycle when the platform cannot deploy
#[test]
fn lifecycle_test_subscription_with_no_offering() {
	new_test_ext().execute_with(|| {
		const SUBSCRIPTION_FUNDING: u64 = 1000;

		// Create a Subscription for which there is deployment descriptor but no offering

		assert_ok!(SubscriptionModule::create_subscription(
			RuntimeOrigin::signed(101),
			b"1234567890".to_vec(),
			19,
			None,
			Some(1),
			SUBSCRIPTION_FUNDING
		));

		// Check that my balance has been reserved
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			SUBSCRIPTION_FUNDING
		);

		// Since there is no Offering, will be canelled by the network after the award wait time
		let wait_time: u32 =
			<crate::mock::Test as crate::pallet::Config>::AwardProcessWaitTime::get();
		run_next((wait_time + 1).into());

		assert_eq!(
			SubscriptionModule::subscriptions_status(101),
			Some(SubscriptionState::Cancelled { reason: TerminationReason::TerminatedByNetwork })
		);

		// Despite cancellation my balance is still locked
		// Check that my balance has been reserved
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			SUBSCRIPTION_FUNDING
		);

		// I need to delete the subscription to get my money back
		assert_ok!(SubscriptionModule::delete_subscription(RuntimeOrigin::signed(101)));

		// Now my money is back
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			0
		);
	});
}

// Lifecycle when the platform can deploy
#[test]
fn lifecycle_test_subscription_with_offering() {
	new_test_ext().execute_with(|| {
		const SUBSCRIPTION_FUNDING: u64 = 1000;

		// Create a Subscription for which there is deployment descriptor but no offering

		assert_ok!(SubscriptionModule::create_subscription(
			RuntimeOrigin::signed(101),
			b"1234567890".to_vec(),
			1,
			None,
			Some(1),
			SUBSCRIPTION_FUNDING
		));

		// Check that my balance has been reserved
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			SUBSCRIPTION_FUNDING
		);

		// Now there is an able provider with an offering, so the deployment will go ahead
		let wait_time: u32 =
			<crate::mock::Test as crate::pallet::Config>::AwardProcessWaitTime::get();
		run_to((wait_time + 1).into());

		assert_eq!(
			SubscriptionModule::subscriptions_status(101),
			Some(SubscriptionState::Deployed { provider: 1 })
		);

		// I cannot delete my subscription because it is deployed, needs settlement first
		assert_noop!(
			SubscriptionModule::delete_subscription(RuntimeOrigin::signed(101)),
			Error::<Test>::InvalidSubscriptionState
		);

		// We wait for the subscription to terminate automatically
		// we can run for nearly 100 blocks as they are price 10 per block but we also
		// paid for deposit, so will be short of that
		let can_afford = SUBSCRIPTION_FUNDING / FLAT_RATE - 1;
		run_next(can_afford);

		// Now it should be terminated
		// The network actually settled itself
		assert_eq!(
			SubscriptionModule::subscriptions_status(101),
			Some(SubscriptionState::Terminated {
				provider: 1,
				reason: TerminationReason::TerminatedByNetwork
			})
		);

		// Did the provider get the funds?
		let paid_amount = can_afford * FLAT_RATE;
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::free_balance(1),
			PROVIDER_FUNDING + paid_amount
		);

		// Now my subscriber reserve is almost gone
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			SUBSCRIPTION_FUNDING - paid_amount
		);

		// To get the rest I need to delete the subscription
		assert_ok!(SubscriptionModule::delete_subscription(RuntimeOrigin::signed(101)));

		// Now my money all my balance is unlocked
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			0
		);

		// Now my money all my balance is unlocked
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::free_balance(101),
			SUBSCRIBER_FUNDING - paid_amount
		);
	});
}

// Lifecycle when the platform can deploy and early termination
#[test]
fn lifecycle_test_subscription_with_offering_early_subscriber_cancellation() {
	new_test_ext().execute_with(|| {
		const SUBSCRIPTION_FUNDING: u64 = 1000;

		// Create a Subscription for which there is deployment descriptor but no offering

		assert_ok!(SubscriptionModule::create_subscription(
			RuntimeOrigin::signed(101),
			b"1234567890".to_vec(),
			1,
			None,
			Some(1),
			SUBSCRIPTION_FUNDING
		));

		// Check that my balance has been reserved
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			SUBSCRIPTION_FUNDING
		);

		// Now there is an able provider with an offering, so the deployment will go ahead
		let wait_time: u32 =
			<crate::mock::Test as crate::pallet::Config>::AwardProcessWaitTime::get();
		run_to((wait_time + 1).into());

		assert_eq!(
			SubscriptionModule::subscriptions_status(101),
			Some(SubscriptionState::Deployed { provider: 1 })
		);

		// We let the subscription run for 10 blocks and then cancel
		let run_for = 10u64;
		run_next(run_for);

		// terminate early
		assert_ok!(SubscriptionModule::terminate_subscription(RuntimeOrigin::signed(101), 101));

		// Now it should be terminated
		// The network actually settled itself
		assert_eq!(
			SubscriptionModule::subscriptions_status(101),
			Some(SubscriptionState::Terminated {
				provider: 1,
				reason: TerminationReason::TerminatedBySubscriber
			})
		);

		// Did the provider get the funds?
		let paid_amount = run_for * FLAT_RATE;
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::free_balance(1),
			PROVIDER_FUNDING + paid_amount
		);

		// Now my subscriber reserve is reduced
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			SUBSCRIPTION_FUNDING - paid_amount
		);

		// To get the rest I need to delete the subscription
		assert_ok!(SubscriptionModule::delete_subscription(RuntimeOrigin::signed(101)));

		// Now my money all my balance is unlocked
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			0
		);

		// Now my money all my balance is unlocked
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::free_balance(101),
			SUBSCRIBER_FUNDING - paid_amount
		);
	});
}

#[test]
fn lifecycle_test_subscription_with_offering_early_provider_cancellation() {
	new_test_ext().execute_with(|| {
		const SUBSCRIPTION_FUNDING: u64 = 1000;

		// Create a Subscription for which there is deployment descriptor but no offering

		assert_ok!(SubscriptionModule::create_subscription(
			RuntimeOrigin::signed(101),
			b"1234567890".to_vec(),
			1,
			None,
			Some(1),
			SUBSCRIPTION_FUNDING
		));

		// Check that my balance has been reserved
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			SUBSCRIPTION_FUNDING
		);

		// Now there is an able provider with an offering, so the deployment will go ahead
		let wait_time: u32 =
			<crate::mock::Test as crate::pallet::Config>::AwardProcessWaitTime::get();
		run_to((wait_time + 1).into());

		assert_eq!(
			SubscriptionModule::subscriptions_status(101),
			Some(SubscriptionState::Deployed { provider: 1 })
		);

		// We let the subscription run for 10 blocks and then cancel
		let run_for = 10u64;
		run_next(run_for);

		// terminate early
		assert_ok!(SubscriptionModule::terminate_subscription(RuntimeOrigin::signed(1), 101));

		// Now it should be terminated
		// The network actually settled itself
		assert_eq!(
			SubscriptionModule::subscriptions_status(101),
			Some(SubscriptionState::Terminated {
				provider: 1,
				reason: TerminationReason::TerminatedByProvider
			})
		);

		// Did the provider get the funds?
		let paid_amount = run_for * FLAT_RATE;
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::free_balance(1),
			PROVIDER_FUNDING + paid_amount
		);

		// Now my subscriber reserve is reduced
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			SUBSCRIPTION_FUNDING - paid_amount
		);

		// To get the rest I need to delete the subscription
		assert_ok!(SubscriptionModule::delete_subscription(RuntimeOrigin::signed(101)));

		// Now my money all my balance is unlocked
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			0
		);

		// Now my money all my balance is unlocked
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::free_balance(101),
			SUBSCRIBER_FUNDING - paid_amount
		);
	});
}

// Lifecycle with intermediate settlement triggered by the provider
#[test]
fn lifecycle_test_subscription_with_offering_intermediate_settlement() {
	new_test_ext().execute_with(|| {
		const SUBSCRIPTION_FUNDING: u64 = 1000;

		// Create a Subscription for which there is deployment descriptor but no offering

		assert_ok!(SubscriptionModule::create_subscription(
			RuntimeOrigin::signed(101),
			b"1234567890".to_vec(),
			1,
			None,
			Some(1),
			SUBSCRIPTION_FUNDING
		));

		// Check that my balance has been reserved
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			SUBSCRIPTION_FUNDING
		);

		// Now there is an able provider with an offering, so the deployment will go ahead
		let wait_time: u32 =
			<crate::mock::Test as crate::pallet::Config>::AwardProcessWaitTime::get();
		run_to((wait_time + 1).into());

		assert_eq!(
			SubscriptionModule::subscriptions_status(101),
			Some(SubscriptionState::Deployed { provider: 1 })
		);

		// do
		run_next(10);

		// request intermediate settlement
		assert_ok!(SubscriptionModule::settle_subscription(RuntimeOrigin::signed(1), 101));

		// Status has not changed
		assert_eq!(
			SubscriptionModule::subscriptions_status(101),
			Some(SubscriptionState::Deployed { provider: 1 })
		);

		// Check intermediate economics
		let intermediate_payment = 10 * FLAT_RATE;

		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::free_balance(1),
			PROVIDER_FUNDING + intermediate_payment
		);

		// Now my subscriber reserve is almost gone
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			SUBSCRIPTION_FUNDING - intermediate_payment
		);

		// We wait for the subscription to terminate automatically
		// we can run for nearly 100 blocks as they are price 10 per block but we also
		// paid for deposit, so will be short of that
		let can_afford = SUBSCRIPTION_FUNDING / FLAT_RATE - 1;
		run_next(can_afford - 10);

		// Now it should be terminated
		// The network actually settled itself
		assert_eq!(
			SubscriptionModule::subscriptions_status(101),
			Some(SubscriptionState::Terminated {
				provider: 1,
				reason: TerminationReason::TerminatedByNetwork
			})
		);

		// Did the provider get the funds?
		let paid_amount = can_afford * FLAT_RATE;
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::free_balance(1),
			PROVIDER_FUNDING + paid_amount
		);

		// Now my subscriber reserve is almost gone
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			SUBSCRIPTION_FUNDING - paid_amount
		);

		// To get the rest I need to delete the subscription
		assert_ok!(SubscriptionModule::delete_subscription(RuntimeOrigin::signed(101)));

		// Now my money all my balance is unlocked
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			0
		);

		// Now my money all my balance is unlocked
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::free_balance(101),
			SUBSCRIBER_FUNDING - paid_amount
		);
	});
}

#[test]
fn lifecycle_test_subscription_with_offering_intermediate_additional_funding() {
	new_test_ext().execute_with(|| {
		const SUBSCRIPTION_FUNDING: u64 = 1000;

		// Create a Subscription for which there is deployment descriptor but no offering

		assert_ok!(SubscriptionModule::create_subscription(
			RuntimeOrigin::signed(101),
			b"1234567890".to_vec(),
			1,
			None,
			Some(1),
			SUBSCRIPTION_FUNDING
		));

		// Check that my balance has been reserved
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			SUBSCRIPTION_FUNDING
		);

		// Now there is an able provider with an offering, so the deployment will go ahead
		let wait_time: u32 =
			<crate::mock::Test as crate::pallet::Config>::AwardProcessWaitTime::get();
		run_to((wait_time + 1).into());

		assert_eq!(
			SubscriptionModule::subscriptions_status(101),
			Some(SubscriptionState::Deployed { provider: 1 })
		);

		// do
		run_next(10);

		// request additional funding, for 10 extra blocks
		assert_ok!(SubscriptionModule::fund_subscription(
			RuntimeOrigin::signed(101),
			10 * FLAT_RATE
		));

		// Status has not changed
		assert_eq!(
			SubscriptionModule::subscriptions_status(101),
			Some(SubscriptionState::Deployed { provider: 1 })
		);

		// Check intermediate economics
		let additional_payment = 10 * FLAT_RATE;

		// Now my subscriber reserve has increased
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			SUBSCRIPTION_FUNDING + additional_payment
		);

		// We wait for the subscription to terminate automatically
		// we can run for nearly 100 blocks as they are price 10 per block but we also
		// paid for deposit, so will be short of that
		// we also extended funds for 10 blocks, after 10 blocks had passed
		let can_afford = (SUBSCRIPTION_FUNDING + additional_payment) / FLAT_RATE - 1;
		run_next(can_afford - 10);

		// Now it should be terminated
		// The network actually settled itself
		assert_eq!(
			SubscriptionModule::subscriptions_status(101),
			Some(SubscriptionState::Terminated {
				provider: 1,
				reason: TerminationReason::TerminatedByNetwork
			})
		);

		// Did the provider get the funds?
		let paid_amount = can_afford * FLAT_RATE;
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::free_balance(1),
			PROVIDER_FUNDING + paid_amount
		);

		// Now my subscriber reserve is almost gone
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			SUBSCRIPTION_FUNDING + additional_payment - paid_amount
		);

		// To get the rest I need to delete the subscription
		assert_ok!(SubscriptionModule::delete_subscription(RuntimeOrigin::signed(101)));

		// Now my money all my balance is unlocked
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::reserved_balance(101),
			0
		);

		// Now my money all my balance is unlocked
		assert_eq!(
			<crate::mock::Test as dinfra_common::DinfraConfig>::Currency::free_balance(101),
			SUBSCRIBER_FUNDING - paid_amount
		);
	});
}
