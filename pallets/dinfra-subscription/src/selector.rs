// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

use codec::{Decode, Encode, MaxEncodedLen};
use scale_info::TypeInfo;
use sp_runtime::RuntimeDebug;

/// Provider Selectors can be reused and are easily identifiable by integer
pub type ProviderSelectorId = u32;

/// We will track the usage of each provider selection entry, as they can be reused in subscriptions
pub type ProviderSelectorUsage = u32;

/// Tracks if a ProviderSelection is in use or not, who a deposit is owned to, and how many of usages there are.
#[derive(Clone, Eq, PartialEq, Encode, Decode, TypeInfo, MaxEncodedLen, RuntimeDebug)]
pub enum ProviderSelectorStatus<AccountId, Balance> {
	NotUsed { deposit: (AccountId, Balance) },
	InUse { deposit: (AccountId, Balance), count: ProviderSelectorUsage },
}
