// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

#![cfg_attr(not(feature = "std"), no_std)]

pub use pallet::*;

#[cfg(test)]
mod mock;

#[cfg(test)]
mod tests;

#[cfg(feature = "runtime-benchmarks")]
mod benchmarking;

mod selector;
mod subscription;

/// The DINFRA subscription pallet allows infrastructure subscribers to specify the deployments they want.
/// so that the network can route them to the most suitable provider.
#[frame_support::pallet]
pub mod pallet {
	use frame_support::{
		dispatch::{DispatchResultWithPostInfo, RawOrigin},
		pallet_prelude::*,
		traits::{
			schedule::{
				v3::{Anon as ScheduleAnon, Named as ScheduleNamed, TaskName},
				DispatchTime, HARD_DEADLINE,
			},
			BalanceStatus, Bounded, LockIdentifier, OriginTrait, QueryPreimage, ReservableCurrency,
			StorePreimage,
		},
	};
	use frame_system::pallet_prelude::*;
	use sp_runtime::{
		traits::{Dispatchable, SaturatedConversion, Zero},
		Saturating,
	};
	use sp_std::vec;

	use super::selector::*;
	use crate::subscription::{
		AwardPreference, SubscriptionFunding, SubscriptionInfo, SubscriptionState,
		TerminationReason,
	};
	use dinfra_common::{traits::*, *};

	/// The provider list of this pallet, bounded by configuration.
	pub type ProviderListOf<T> =
		BoundedVec<ProviderAccountIdOf<T>, <T as Config>::MaxSelectedProviders>;

	/// The awarding preference of this pallet, which uses the Selector ID.
	pub type AwardPreferenceOf<T> = AwardPreference<ProviderSelectorId, ProviderAccountIdOf<T>>;

	/// The SubscriptionInfo tracks the main subscription data.
	pub type SubscriptionInfoOf<T> =
		SubscriptionInfo<DeploymentDescriptorOf<T>, DeploymentTypeId, AwardPreferenceOf<T>>;

	/// The Subscription state tracks the awarding of the subscription
	pub type SubscriptionStateOf<T> = SubscriptionState<ProviderAccountIdOf<T>>;

	/// The subscription funding tracks the payments made, rates and finalization
	pub type SubscriptionFundingOf<T> = SubscriptionFunding<BlockNumberFor<T>, BalanceOf<T>>;

	/// The type of (Scheduled) Calls for this Pallet
	pub type CallOf<T> = <T as Config>::RuntimeCall;
	pub type PalletsOriginOf<T> =
		<<T as frame_system::Config>::RuntimeOrigin as OriginTrait>::PalletsOrigin;

	pub type BoundedCallOf<T> = Bounded<<T as Config>::RuntimeCall>;

	pub const SUBSCRIPTION_ID: LockIdentifier = *b"subscrit";

	/// Configure the pallet by specifying the parameters and types on which it depends.
	#[pallet::config]
	pub trait Config: frame_system::Config + DinfraConfig {
		/// Because this pallet emits events, it depends on the runtime's definition of an event.
		type RuntimeEvent: From<Event<Self>> + IsType<<Self as frame_system::Config>::RuntimeEvent>;

		/// Deposit required to create Subscriptions
		#[pallet::constant]
		type SubscriptionDeposit: Get<BalanceOf<Self>>;

		/// The maximum amount of providers that can be selected
		type MaxSelectedProviders: Get<u32>;

		/// The period before starting the award process
		type AwardProcessWaitTime: Get<u32>;

		type RuntimeCall: Parameter
			+ Dispatchable<RuntimeOrigin = Self::RuntimeOrigin>
			+ From<Call<Self>>
			+ IsType<<Self as frame_system::Config>::RuntimeCall>
			+ From<frame_system::Call<Self>>;

		/// The Scheduler for dispatching contracts
		type Scheduler: ScheduleAnon<BlockNumberFor<Self>, CallOf<Self>, PalletsOriginOf<Self>>
			+ ScheduleNamed<BlockNumberFor<Self>, CallOf<Self>, PalletsOriginOf<Self>>;

		/// The preimage provider.
		type Preimages: QueryPreimage + StorePreimage;

		/// The dinfra Provider pallet.
		type Provider: Provider<Self>;
	}

	#[pallet::pallet]
	pub struct Pallet<T>(_);

	/// The store for provider selectors
	#[pallet::storage]
	#[pallet::getter(fn provider_selectors)]
	pub type ProviderSelectors<T> =
		StorageMap<_, Twox64Concat, ProviderSelectorId, ProviderSelectorInfo<ProviderListOf<T>>>;

	/// The store for the status of the provider selectors
	#[pallet::storage]
	#[pallet::getter(fn provider_selectors_status)]
	pub type ProviderSelectorsStatus<T> = StorageMap<
		_,
		Twox64Concat,
		ProviderSelectorId,
		ProviderSelectorStatus<AccountIdOf<T>, BalanceOf<T>>,
	>;

	/// The store for the subscriptions
	/// The subscription is identified by its owner, the subscriber.
	/// One subscriber can only have one active subscription at the time.
	#[pallet::storage]
	#[pallet::getter(fn subscriptions)]
	pub type Subscriptions<T> =
		StorageMap<_, Identity, SubscriberAccountIdOf<T>, SubscriptionInfoOf<T>>;

	/// The store for the subscriptions status.
	#[pallet::storage]
	#[pallet::getter(fn subscriptions_status)]
	pub type SubscriptionsState<T> =
		StorageMap<_, Identity, SubscriberAccountIdOf<T>, SubscriptionStateOf<T>>;

	/// The store for the subscriptions funding status.
	#[pallet::storage]
	#[pallet::getter(fn subscriptions_funding)]
	pub type SubscriptionsFunding<T> =
		StorageMap<_, Identity, SubscriberAccountIdOf<T>, SubscriptionFundingOf<T>>;

	// Pallets use events to inform users when important changes are made.
	#[pallet::event]
	#[pallet::generate_deposit(pub(super) fn deposit_event)]
	pub enum Event<T: Config> {
		/// A ProviderSelector was Created [selector_id,creator,deposit]
		ProviderSelectorCreated(ProviderSelectorId, SubscriberAccountIdOf<T>, BalanceOf<T>),
		/// A Subscription has been created [subscription_id, deployment_type]
		SubscriptionCreated(SubscriberAccountIdOf<T>, DeploymentTypeId),
		/// A Subscription has been updated [subscription_id, state]
		SubscriptionUpdated(
			SubscriberAccountIdOf<T>,
			SubscriptionStateOf<T>,
			SubscriptionFundingOf<T>,
		),
		/// A Bubscription has been awarded
		SubscriptionAwarded(
			SubscriberAccountIdOf<T>,
			SubscriptionStateOf<T>,
			SubscriptionFundingOf<T>,
		),
		/// A Bubscription has been deployed
		SubscriptionDeployed(
			SubscriberAccountIdOf<T>,
			SubscriptionStateOf<T>,
			SubscriptionFundingOf<T>,
		),
		/// A subscription was terminated
		SubscriptionTerminated(
			SubscriberAccountIdOf<T>,
			SubscriptionStateOf<T>,
			SubscriptionFundingOf<T>,
		),
		/// A subscription was deleted
		SubscriptionDeleted(SubscriberAccountIdOf<T>),
	}

	// Errors inform users that something went wrong.
	#[pallet::error]
	pub enum Error<T> {
		/// Unreachable code has been reaced somehow, there is a bug in this pallet.
		PalletImplementationError,
		/// The provider list is too long.
		ProviderListTooLong,
		/// The selector id is already in use.
		SelectorIdInUse,
		/// The selector id does not exists.
		SelectorIdDoesNotExist,
		/// The Deployment Descriptor CID is too long
		CIDTooLong,
		/// Either a Provider or a Selector needs to be provided, but not both.
		SelectorOrProviderRequired,
		/// The provider selector was not found.
		ProviderSelectorNotFound,
		/// The subscriber already has a subscription in place.
		SubscriptionAlreadyExists,
		/// The Subscription does not exist
		SubscriptionDoesNotExist,
		/// You are not allowed to perform the requested action.
		PermissionDenied,
		/// The Subscription was already terminated.
		AlreadyTerminated,
		/// Invalid Deployment Type ID
		InvalidDeploymentTypeId,
		/// Provided funding is not Enough
		NotEnoughFunding,
		/// The subscription is not in a valid state
		InvalidSubscriptionState,
	}

	#[pallet::hooks]
	impl<T: Config> Hooks<BlockNumberFor<T>> for Pallet<T> {}

	// Dispatchable functions allows users to interact with the pallet and invoke state changes.
	// These functions materialize as "extrinsics", which are often compared to transactions.
	// Dispatchable functions must be annotated with a weight and must return a DispatchResult.
	#[pallet::call]
	impl<T: Config> Pallet<T> {
		/// Creates a provider selector. Selectors can be reused in many subscriptions, required a
		/// deposit to be created, its usage is tracked. Currently explicit selection is supported
		/// only, but other methods are potencially possible.
		#[pallet::call_index(1)]
		#[pallet::weight(Weight::from_parts(10_000, 0) + T::DbWeight::get().reads_writes(1,2))]
		pub fn create_provider_selector(
			origin: OriginFor<T>,
			provider_selector_id: ProviderSelectorId,
			list_type: ProviderListType,
			provider_list: ProviderList<ProviderAccountIdOf<T>>,
		) -> DispatchResultWithPostInfo {
			// Check the signature
			let who = ensure_signed(origin)?;

			// Validate the list length and bound it
			let blist: ProviderListOf<T> =
				provider_list.to_vec().try_into().map_err(|_| Error::<T>::ProviderListTooLong)?;

			// Ensure the selector does not exist
			ensure!(
				!ProviderSelectors::<T>::contains_key(provider_selector_id),
				Error::<T>::SelectorIdInUse
			);

			// capture the deposit
			let deposit = T::SubscriptionDeposit::get();
			T::Currency::reserve(&who, deposit)?;

			// TODO: only explicit selector supported for now
			// create the selector
			<ProviderSelectors<T>>::insert(
				provider_selector_id,
				ProviderSelectorInfo::<ProviderListOf<T>> {
					explicit: Some((list_type, blist)),
					regional: None,
					network: None,
					qualification: None,
				},
			);

			// create the selector status status
			<ProviderSelectorsStatus<T>>::insert(
				provider_selector_id,
				ProviderSelectorStatus::NotUsed { deposit: (who.clone(), deposit) },
			);

			// Notify the creation of the deployment type
			Self::deposit_event(Event::ProviderSelectorCreated(provider_selector_id, who, deposit));

			// Return a successful DispatchResultWithPostInfo
			Ok(().into())
		}

		/// Creates a subscription, which is basically a request for infrastructure, providing
		/// all preferences that make possible its deployment.
		#[pallet::call_index(2)]
		#[pallet::weight(Weight::from_parts(10_000, 0) + T::DbWeight::get().reads_writes(1,2))]
		pub fn create_subscription(
			origin: OriginFor<T>,
			deployment_descriptor_cid: CID,
			deployment_type: DeploymentTypeId,
			provider_selector_id: Option<ProviderSelectorId>,
			explicit_provider: Option<ProviderAccountIdOf<T>>,
			initial_funding: BalanceOf<T>,
		) -> DispatchResultWithPostInfo {
			// Check signature
			let who = ensure_signed(origin)?;

			// check CID limits
			let bounded_dd: DeploymentDescriptorOf<T> =
				deployment_descriptor_cid.try_into().map_err(|_| Error::<T>::CIDTooLong)?;

			// ensure the initial funding is enough
			// minimum is the deposit size,
			let minimun_funding = T::SubscriptionDeposit::get();
			ensure!(initial_funding >= minimun_funding, Error::<T>::NotEnoughFunding);
			T::Currency::reserve(&who, initial_funding)?;

			// check one of either selector or provider, not both
			(matches!(
				(provider_selector_id, &explicit_provider),
				(Some(_), None) | (None, Some(_))
			))
			.then(|| ())
			.ok_or_else(|| Error::<T>::SelectorOrProviderRequired)?;

			// check subscriber has no active subscription
			ensure!(!Subscriptions::<T>::contains_key(&who), Error::<T>::SubscriptionAlreadyExists);

			// Check that the provider selector exists
			if provider_selector_id.is_some() {
				ensure!(
					ProviderSelectors::<T>::contains_key(provider_selector_id.unwrap()),
					Error::<T>::ProviderSelectorNotFound
				);
			}

			// Check that the DeploymentTypeIsValid
			T::Provider::contains_deployment_type_id(deployment_type)
				.then(|| ())
				.ok_or_else(|| Error::<T>::InvalidDeploymentTypeId)?;

			// the awarding preference
			let award_preference = match (provider_selector_id, &explicit_provider) {
				(Some(s), None) => AwardPreferenceOf::<T>::Selection { selector: s },
				(None, Some(p)) => AwardPreferenceOf::<T>::Explicit { provider: p.clone() },
				_ => unreachable!("Impossible execution path"),
			};

			// create the subscription
			<Subscriptions<T>>::insert(
				who.clone(),
				SubscriptionInfoOf::<T> {
					deployment_descriptor: bounded_dd,
					deployment_type,
					award_preference,
				},
			);

			// create the subscription status
			<SubscriptionsState<T>>::insert(who.clone(), SubscriptionState::Created);

			// create the subscription funding
			<SubscriptionsFunding<T>>::insert(
				who.clone(),
				SubscriptionFunding {
					// We are not deployed yet
					deployed_at: None,
					paid_until: None,
					// Funding provided, we keep the deposit until data structures are freed
					remaining_funds: initial_funding - minimun_funding,
					// Not priced yet
					rate: Rate { rate_type: RateType::None, rate: 0u32.into() },
				},
			);

			// Schedule the award process
			// We don't want this process to be atomic, because in the future it may involve
			// Attempting deployments with the selected providers

			Self::do_schedule(
				(SUBSCRIPTION_ID, "award", &who).using_encoded(sp_io::hashing::blake2_256),
				DispatchTime::After(T::AwardProcessWaitTime::get().into()), // We start in a minute
				CallOf::<T>::from(crate::Call::award_subscription { subscriber: who.clone() }),
			);

			// lock funding, no need to get a deposit, but minimum funding is just like a deposit.
			// It is possible to run free

			Self::deposit_event(Event::SubscriptionCreated(who.clone(), deployment_type));

			// Return a successful DispatchResultWithPostInfo
			Ok(().into())
		}

		/// Terminates a subscription,
		/// Subscriptions can be terminated by the owner the provider or the system.
		/// The system always schedules termination at end of balance block.
		/// Subscriptions are identified by the owning account.
		#[pallet::call_index(3)]
		#[pallet::weight(Weight::from_parts(10_000, 0) + T::DbWeight::get().reads_writes(1,2))]
		pub fn terminate_subscription(
			origin: OriginFor<T>,
			subscription_owner: SubscriberAccountIdOf<T>,
		) -> DispatchResultWithPostInfo {
			// Get the Subscription State
			// And ensure origin is a party
			let (sender, state) =
				Self::ensure_subscription_party(origin, subscription_owner.clone())?;

			let new_state = match (sender, state.clone()) {
				(Some(s), SubscriptionState::Created | SubscriptionState::Rejected)
					if s == subscription_owner =>
					SubscriptionState::Cancelled {
						reason: TerminationReason::TerminatedBySubscriber,
					},
				(None, SubscriptionState::Created | SubscriptionState::Rejected) =>
					SubscriptionState::Cancelled { reason: TerminationReason::TerminatedByNetwork },
				(
					Some(s),
					SubscriptionState::Awarded { provider: p } |
					SubscriptionState::Deployed { provider: p },
				) if s == subscription_owner => SubscriptionState::Terminated {
					reason: TerminationReason::TerminatedBySubscriber,
					provider: p,
				},
				(
					Some(s),
					SubscriptionState::Awarded { provider: p } |
					SubscriptionState::Deployed { provider: p },
				) if s == p => SubscriptionState::Terminated {
					reason: TerminationReason::TerminatedByProvider,
					provider: p,
				},
				(
					None,
					SubscriptionState::Awarded { provider: p } |
					SubscriptionState::Deployed { provider: p },
				) => SubscriptionState::Terminated {
					reason: TerminationReason::TerminatedByNetwork,
					provider: p,
				},
				(_, SubscriptionState::Terminated { .. }) =>
					return Err(Error::<T>::AlreadyTerminated.into()),
				_ => return Err(Error::<T>::PermissionDenied.into()),
			};

			// Update the State

			<SubscriptionsState<T>>::insert(subscription_owner.clone(), &new_state);

			// Depending on state there will be something scheduled
			let schedule_id = match state {
				// Cancelling before the award process
				SubscriptionState::Created => Some(
					(SUBSCRIPTION_ID, "award", &subscription_owner)
						.using_encoded(sp_io::hashing::blake2_256),
				),
				// Award when deployed and waiting termination
				SubscriptionState::Deployed { .. } => Some(
					(SUBSCRIPTION_ID, "termination", &subscription_owner)
						.using_encoded(sp_io::hashing::blake2_256),
				),
				_ => None,
			};

			// Unschedule if there is an schedule to cancel
			if schedule_id.is_some() {
				Self::do_unschedule(schedule_id.unwrap());
			}

			// Settles the subscription
			let mut subscription_funding = SubscriptionsFunding::<T>::get(&subscription_owner)
				.ok_or(Error::<T>::PalletImplementationError)?;
			let _settled = Self::do_settle_subscription(
				subscription_owner.clone(),
				new_state.clone(),
				&mut subscription_funding,
			)?;

			let subscription_info = <Subscriptions<T>>::get(&subscription_owner).unwrap();

			// Let the provider know, so that capacity gets updated
			// this is only required when the award actually took place

			let provider = match state {
				SubscriptionState::Awarded { provider: p } |
				SubscriptionState::Deployed { provider: p } |
				SubscriptionState::Terminated { provider: p, .. } => Some(p),
				_ => None,
			};

			if provider.is_some() {
				T::Provider::terminate_deployment(
					subscription_info.deployment_type,
					provider.unwrap(),
					subscription_owner.clone(),
				);
			}

			// Signal Events
			Self::deposit_event(Event::SubscriptionTerminated(
				subscription_owner,
				new_state,
				subscription_funding,
			));

			// Return a successful DispatchResultWithPostInfo
			Ok(().into())
		}

		/// Initiates the award process
		/// This extrinsic can only be called by Root
		#[pallet::call_index(4)]
		#[pallet::weight(Weight::from_parts(10_000, 0) + T::DbWeight::get().reads_writes(1,2))]
		pub fn award_subscription(
			origin: OriginFor<T>,
			subscriber: SubscriberAccountIdOf<T>,
		) -> DispatchResultWithPostInfo {
			// Only root can  trigger this process
			ensure_root(origin)?;

			// Get the subscription information
			let subscription = Subscriptions::<T>::get(&subscriber);
			ensure!(subscription.is_some(), Error::<T>::SubscriptionDoesNotExist);

			let subscription = subscription.unwrap();
			let award_result = T::Provider::award_deployment(
				subscription.deployment_type,
				subscription.deployment_descriptor.into(),
				subscriber.clone(),
				Self::award_preference_to_selector(subscription.award_preference),
			);

			match award_result {
				// If the subscription was not awarded the network will terminate it
				Err(_) => Self::terminate_subscription(RawOrigin::Root.into(), subscriber),
				// The Subscription was awarded, so we need to schedule automatic termination
				Ok((provider, rate)) => {
					// award to provider
					// TODO: The MVP assumes that Award implies Deployment
					// It would be advisable to allow the provider to report if the deployment was
					// achieved. Having an additional step here.
					let new_state = SubscriptionState::Deployed { provider };
					SubscriptionsState::<T>::insert(&subscriber, &new_state);

					// signal that the subscription has started now
					// TODO: the MVP does not take into account that the provider could not be able to deploy
					// there could be a back and forth across providers in the selector until deployment
					let now = frame_system::Pallet::<T>::block_number();

					// Update the Subscription Funding,
					let subscription_funding =
						match SubscriptionsFunding::<T>::get(&subscriber).unwrap() {
							SubscriptionFunding { remaining_funds: f, .. } => SubscriptionFunding {
								remaining_funds: f,
								deployed_at: Some(now),
								paid_until: Some(now),
								rate,
							},
						};
					SubscriptionsFunding::<T>::insert(&subscriber, &subscription_funding);

					// Notify Subscription Update

					Self::deposit_event(Event::SubscriptionAwarded(
						subscriber.clone(),
						new_state.clone(),
						subscription_funding.clone(),
					));

					// Schedule Subscription Termination

					Self::expected_termination(subscription_funding.clone()).map(|termination|
						// termination is scheduled only for paid subscriptions
					  	Self::do_schedule(
							(SUBSCRIPTION_ID, "termination", &subscriber).using_encoded(sp_io::hashing::blake2_256),
							termination,
							CallOf::<T>::from(crate::Call::terminate_subscription {
								subscription_owner: subscriber.clone(),
							}),
						));

					// Notify changes in the subscription
					Self::deposit_event(Event::SubscriptionUpdated(
						subscriber,
						new_state,
						subscription_funding,
					));

					Ok(().into())
				},
			}
		}

		/// Settles the Subscription by transferring transferring consumed balances to the provider
		/// Can be called by subscriber, provider and root
		#[pallet::call_index(5)]
		#[pallet::weight(Weight::from_parts(10_000, 0) + T::DbWeight::get().reads_writes(1,2))]
		pub fn settle_subscription(
			origin: OriginFor<T>,
			subscriber: SubscriberAccountIdOf<T>,
		) -> DispatchResultWithPostInfo {
			// Get the Subscription State
			// And ensure origin is a party
			let (_sender, subscription_state) =
				Self::ensure_subscription_party(origin, subscriber.clone())?;

			let mut subscription_funding = SubscriptionsFunding::<T>::get(&subscriber)
				.ok_or(Error::<T>::PalletImplementationError)?;

			let settled = Self::do_settle_subscription(
				subscriber.clone(),
				subscription_state.clone(),
				&mut subscription_funding,
			)?;

			if settled {
				Self::deposit_event(Event::SubscriptionUpdated(
					subscriber,
					subscription_state,
					subscription_funding,
				));
			}

			// Return a successful DispatchResultWithPostInfo
			Ok(().into())
		}

		/// Deletes a subscription after it has been settled.
		/// Only the subscriber can do the final deletion
		/// Remaining locked funds are returned
		#[pallet::call_index(6)]
		#[pallet::weight(Weight::from_parts(10_000, 0) + T::DbWeight::get().reads_writes(1,2))]
		pub fn delete_subscription(origin: OriginFor<T>) -> DispatchResultWithPostInfo {
			// Check signature
			let subscriber = ensure_signed(origin)?;

			let state = SubscriptionsState::<T>::get(&subscriber);

			// Ensure the subscription exists
			ensure!(state.is_some(), Error::<T>::SubscriptionDoesNotExist);

			// Ensure the subscription is in the right state
			match state.unwrap() {
				SubscriptionState::Terminated { .. } => Ok(()),
				SubscriptionState::Cancelled { .. } => Ok(()),
				_ => Err(Error::<T>::InvalidSubscriptionState),
			}?;

			// In this state all balances must have been already settled, and all remaining funds are returned
			let funding = SubscriptionsFunding::<T>::get(&subscriber)
				.ok_or(Error::<T>::PalletImplementationError)?;

			//Note that we need to return Deposit and Pending funds
			let funds = match funding {
				SubscriptionFunding { remaining_funds: r, .. } => r,
			};

			let deposit = T::SubscriptionDeposit::get();
			T::Currency::unreserve(&subscriber, deposit + funds);

			// Delete all Subscription Objects
			SubscriptionsFunding::<T>::remove(&subscriber);
			SubscriptionsState::<T>::remove(&subscriber);
			Subscriptions::<T>::remove(&subscriber);

			Self::deposit_event(Event::SubscriptionDeleted(subscriber));

			// Return a successful DispatchResultWithPostInfo
			Ok(().into())
		}

		/// Provides additional funding to a subscription
		#[pallet::call_index(7)]
		#[pallet::weight(Weight::from_parts(10_000, 0) + T::DbWeight::get().reads_writes(1,2))]
		pub fn fund_subscription(
			origin: OriginFor<T>,
			funding: BalanceOf<T>,
		) -> DispatchResultWithPostInfo {
			// Check signature
			let subscriber = ensure_signed(origin)?;

			let state = SubscriptionsState::<T>::get(&subscriber)
				.ok_or(Error::<T>::SubscriptionDoesNotExist)?;

			// Ensure the subscription is in the right state
			match state {
				SubscriptionState::Awarded { .. } => Ok(()),
				SubscriptionState::Deployed { .. } => Ok(()),
				SubscriptionState::Created { .. } => Ok(()),
				_ => Err(Error::<T>::InvalidSubscriptionState),
			}?;

			let mut subscription_funding = SubscriptionsFunding::<T>::get(&subscriber)
				.ok_or(Error::<T>::PalletImplementationError)?;

			// Make the additional reserve
			T::Currency::reserve(&subscriber, funding)?;
			subscription_funding.remaining_funds += funding;

			// Update the funding
			<SubscriptionsFunding<T>>::insert(&subscriber, &subscription_funding);

			// Reschedule the termination, if it proceeds
			Self::expected_termination(subscription_funding.clone()).map(|termination|
				// termination is scheduled only for paid subscriptions
				Self::do_reschedule(
					(SUBSCRIPTION_ID, "termination", &subscriber).using_encoded(sp_io::hashing::blake2_256),
					termination,
				));

			// Notify

			// Notify changes in the subscription
			Self::deposit_event(Event::SubscriptionUpdated(
				subscriber,
				state,
				subscription_funding,
			));
			// Return a successful DispatchResultWithPostInfo
			Ok(().into())
		}

		// TODO: fund_subscription
	}

	/// Helper Functions
	impl<T: Config> Pallet<T> {
		// Converts and Award Preference to a Provider Selector as understood by the provider
		// The distinction is because an AwardPreference may refer to an stored
		// Provider Selector, which allows them to be reused
		fn award_preference_to_selector(
			award_preference: AwardPreferenceOf<T>,
		) -> ProviderSelectorInfo<ProviderList<ProviderAccountIdOf<T>>> {
			match award_preference {
				// If the preference is explicit provider we return a selector of one
				AwardPreference::Explicit { provider: p } => ProviderSelectorInfo {
					explicit: Some((ProviderListType::Select, vec![p])),
					regional: None,
					network: None,
					qualification: None,
				},
				// If the preference is a stored selector, we just return it
				AwardPreference::Selection { selector: s } => match ProviderSelectors::<T>::get(s)
					.unwrap()
				{
					ProviderSelectorInfo { explicit: Some((lt, ps)), .. } => ProviderSelectorInfo {
						explicit: Some((lt, ps.into())),
						regional: None,
						network: None,
						qualification: None,
					},
					_ => unreachable!("Impossible provider selector"),
				},
			}
		}

		// Ensure that the origin is a party to the subscription
		// Provider, Subsciber or Root are considered valid parties
		fn ensure_subscription_party(
			origin: OriginFor<T>,
			subscription_owner: SubscriberAccountIdOf<T>,
		) -> Result<(Option<AccountIdOf<T>>, SubscriptionStateOf<T>), Error<T>> {
			// Return the subscription state
			let state = SubscriptionsState::<T>::get(&subscription_owner);

			// Check this is a valid origin: owner, provider or root.
			match (origin.into(), &state) {
				(_, None) => Err(Error::<T>::SubscriptionDoesNotExist),
				(Ok(RawOrigin::Root), Some(_)) => Ok((None, state.unwrap())),
				(Ok(RawOrigin::Signed(who)), Some(_)) if who == subscription_owner =>
					Ok((Some(who), state.unwrap())),
				(
					Ok(RawOrigin::Signed(who)),
					Some(
						SubscriptionState::Awarded { provider: p } |
						SubscriptionState::Deployed { provider: p } |
						SubscriptionState::Terminated { provider: p, .. },
					),
				) if who == *p => Ok((Some(who), state.unwrap())),
				_ => Err(Error::<T>::PermissionDenied),
			}
		}

		// Simple Scheduling of subscription tasks
		pub(crate) fn do_schedule(
			id: TaskName,
			when: DispatchTime<BlockNumberFor<T>>,
			call: CallOf<T>,
		) {
			let bound_call = T::Preimages::bound(call);
			let ok = T::Scheduler::schedule_named(
				id,
				when,
				None,
				HARD_DEADLINE,
				RawOrigin::Root.into(),
				bound_call.unwrap(),
			)
			.is_ok();
			debug_assert!(ok, "LOGIC ERROR: could not schedule subscription task");
		}

		// Simple Re-Scheduling of subscription tasks
		pub(crate) fn do_reschedule(id: TaskName, when: DispatchTime<BlockNumberFor<T>>) {
			let ok = T::Scheduler::reschedule_named(id, when).is_ok();
			debug_assert!(ok, "LOGIC ERROR: could not reschedule subscription task");
		}

		// Simple Un-Scheduling of subscription tasks
		pub(crate) fn do_unschedule(id: TaskName) {
			// TODO: depending on cases schedule was or not set before termination
			// Terminated by Network, for example
			// a detailed review is required.
			let _ = T::Scheduler::cancel_named(id).is_ok();
		}

		// Returns the expected termination of a SubscriptionFunding
		// TODO: This code is nightmarish, generic types in use can't do anything, defining the types further
		// requires propagation everywhere, etc.
		pub(crate) fn expected_termination(
			subscription_funding: SubscriptionFundingOf<T>,
		) -> Option<DispatchTime<BlockNumberFor<T>>> {
			// If the subscription has not started
			subscription_funding.deployed_at?;

			// Subscription has started

			match subscription_funding.rate {
				Rate { rate_type: RateType::None, .. } => None,
				Rate { rate: r, .. } if r.is_zero() => None,
				Rate { rate: r, .. } => {
					let rb = subscription_funding.remaining_funds / r;
					let remaining_blocks = rb.saturated_into::<u32>();
					let paid_until =
						subscription_funding.paid_until.unwrap().saturated_into::<u32>();
					Some(DispatchTime::At((paid_until + remaining_blocks).into()))
				},
			}
		}

		// The Balance Owed by the Subscriber to the provider
		pub(crate) fn owed_balance(
			subscription_funding: SubscriptionFundingOf<T>,
		) -> Option<(BlockNumberFor<T>, BalanceOf<T>)> {
			let n = frame_system::Pallet::<T>::block_number();

			match subscription_funding {
				// subscriber owes nothing if it was not deployed
				SubscriptionFunding { deployed_at: None, .. } => None,
				// subscribes owes nothing if it was a free subscription
				SubscriptionFunding { rate: Rate { rate_type: RateType::None, .. }, .. } => None,
				SubscriptionFunding { rate: Rate { rate: r, .. }, .. } if r.is_zero() => None,
				SubscriptionFunding {
					paid_until: Some(pu),
					rate: Rate { rate_type: RateType::FixedPerBlock, rate: r },
					..
				} => {
					let paid_until = pu.saturated_into::<u32>();
					let now = n.saturated_into::<u32>();
					let unpaid_blocks = now - paid_until;
					let owed = r.saturating_mul(unpaid_blocks.into());
					Some((n, owed))
				},
				// Other payment types not implemented yet
				_ => None,
			}
		}

		// Calculates what is owned, makes transfers, etc
		// modifies the subscription funding and updates the store
		pub(crate) fn do_settle_subscription(
			subscriber: SubscriberAccountIdOf<T>,
			subscription_state: SubscriptionStateOf<T>,
			subscription_funding: &mut SubscriptionFundingOf<T>,
		) -> Result<bool, DispatchError> {
			match Self::owed_balance(subscription_funding.clone()) {
				// Was not Updated
				None => Ok(false),
				Some((now, owed)) => {
					subscription_funding.paid_until = Some(now);
					subscription_funding.remaining_funds -= owed;
					let provider = match subscription_state {
						SubscriptionState::Deployed { provider: p } |
						SubscriptionState::Terminated { provider: p, .. } => Ok(p),
						_ => Err(Error::<T>::PalletImplementationError),
					}?;
					let _paid = T::Currency::repatriate_reserved(
						&subscriber,
						&provider,
						owed,
						BalanceStatus::Free,
					)?;

					// Update the record
					SubscriptionsFunding::<T>::insert(subscriber, subscription_funding);

					// It Was Updated
					Ok(true)
				},
			}
		}
	}
}
