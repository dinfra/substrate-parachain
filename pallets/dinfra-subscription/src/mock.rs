use frame_support::{
	parameter_types,
	traits::{ConstU64, Everything},
};
use frame_system as system;
use sp_core::H256;
use sp_runtime::{
	traits::{BlakeTwo256, IdentityLookup},
	BuildStorage,
};

use frame_support::{pallet_prelude::*, traits::EqualPrivilegeOnly};
use frame_system::EnsureRoot;

type Block = frame_system::mocking::MockBlock<Test>;

// Some test constats
pub const PROVIDER_FUNDING: u64 = 100;
pub const SUBSCRIBER_FUNDING: u64 = 10000;
pub const FLAT_RATE: u64 = 10;

// Configure a mock runtime to test the pallet.
frame_support::construct_runtime!(
	pub enum Test
	{
		System: frame_system::{Pallet, Call, Config<T>, Storage, Event<T>},
		Balances: pallet_balances::{Pallet, Call, Storage, Config<T>, Event<T>},
		SubscriptionModule: crate::{Pallet, Call, Storage, Event<T>},
		Scheduler: pallet_scheduler,
		Preimage: pallet_preimage,
		Provider: mocked_provider,
	}
);

parameter_types! {
	pub const BlockHashCount: u64 = 250;
	pub const SS58Prefix: u8 = 42;
}

pub mod mocked_provider {
	pub use pallet::*;

	#[frame_support::pallet]
	pub mod pallet {
		use dinfra_common::{traits::*, *};
		use frame_support::pallet_prelude::*;

		#[pallet::pallet]
		pub struct Pallet<T>(_);

		#[pallet::config]
		pub trait Config: frame_system::Config + DinfraConfig {
			type ProviderDeposit: Get<BalanceOf<Self>>;
		}
		impl<T: Config> Provider<T> for Pallet<T> {
			// the 10 first deployment types are deployed, not the rest
			fn contains_deployment_type_id(deployment_type_id: DeploymentTypeId) -> bool {
				match deployment_type_id {
					d if d <= 20 => true,
					_ => false,
				}
			}

			// deployment termination
			fn terminate_deployment(
				_deployment_type_id: DeploymentTypeId,
				_provider: ProviderAccountIdOf<T>,
				_subscriber: SubscriberAccountIdOf<T>,
			) {
				// dont really need to do anything, since in this mock nothing got deployed
			}

			// We simulate hardcoded behaviours based on
			// Provided parameters
			fn award_deployment(
				deployment_type_id: DeploymentTypeId,
				_deployment_descriptor: DeploymentDescriptorOf<T>,
				_subscriber: SubscriberAccountIdOf<T>,
				provider_selection: ProviderSelectorInfo<ProviderList<ProviderAccountIdOf<T>>>,
			) -> Result<(ProviderAccountIdOf<T>, Rate<BalanceOf<T>>), RejectionReason> {
				let r = T::ProviderDeposit::get(); // 10 per block
				let flat_rate = Rate { rate_type: RateType::FixedPerBlock, rate: r };
				match (deployment_type_id, provider_selection) {
					// If there is deployment type id and a provider selector award to the first and assume an offering exists with flat rate
					(
						d,
						ProviderSelectorInfo {
							explicit: Some((ProviderListType::Select, providers)),
							..
						},
					) if d <= 10 => Ok((providers[0].clone(), flat_rate)),
					// If no selector is passed, but it is one of the first 10 deployment types, then assume no offering
					(d, ProviderSelectorInfo { explicit: None, .. }) if d <= 3 =>
						Err(RejectionReason::NoOffering),
					(d, ProviderSelectorInfo { explicit: None, .. }) if d <= 6 =>
						Err(RejectionReason::NoSelectableProvider),
					(d, ProviderSelectorInfo { explicit: None, .. }) if d <= 10 =>
						Err(RejectionReason::NoCapacity),
					_ => Err(RejectionReason::NoOffering),
				}
			}
		}
	}
}

impl system::Config for Test {
	type BaseCallFilter = Everything;
	type BlockWeights = ();
	type BlockLength = ();
	type DbWeight = ();
	type RuntimeOrigin = RuntimeOrigin;
	type RuntimeCall = RuntimeCall;
	type Nonce = u64;
	type Hash = H256;
	type Hashing = BlakeTwo256;
	type AccountId = u64;
	type Lookup = IdentityLookup<Self::AccountId>;
	type Block = Block;
	type RuntimeEvent = RuntimeEvent;
	type BlockHashCount = BlockHashCount;
	type Version = ();
	type PalletInfo = PalletInfo;
	type AccountData = pallet_balances::AccountData<u64>;
	type OnNewAccount = ();
	type OnKilledAccount = ();
	type SystemWeightInfo = ();
	type SS58Prefix = SS58Prefix;
	type OnSetCode = ();
	type MaxConsumers = frame_support::traits::ConstU32<16>;
}

impl pallet_balances::Config for Test {
	type MaxLocks = ();
	type MaxReserves = ();
	type ReserveIdentifier = [u8; 8];
	type Balance = u64;
	type RuntimeEvent = RuntimeEvent;
	type DustRemoval = ();
	type ExistentialDeposit = ConstU64<1>;
	type AccountStore = System;
	type WeightInfo = ();
	type FreezeIdentifier = ();
	type MaxFreezes = ();
	type RuntimeHoldReason = ();
	type MaxHolds = ();
}

impl pallet_preimage::Config for Test {
	type RuntimeEvent = RuntimeEvent;
	type WeightInfo = ();
	type Currency = ();
	type ManagerOrigin = EnsureRoot<u64>;
	type BaseDeposit = ();
	type ByteDeposit = ();
}

parameter_types! {
	pub MaxWeight: Weight = Weight::from_parts(2_000_000_000_000, u64::MAX);
}

impl pallet_scheduler::Config for Test {
	type RuntimeEvent = RuntimeEvent;
	type RuntimeOrigin = RuntimeOrigin;
	type PalletsOrigin = OriginCaller;
	type RuntimeCall = RuntimeCall;
	type MaximumWeight = MaxWeight;
	type ScheduleOrigin = EnsureRoot<u64>;
	type MaxScheduledPerBlock = ConstU32<100>;
	type WeightInfo = ();
	type OriginPrivilegeCmp = EqualPrivilegeOnly;
	type Preimages = ();
}

impl dinfra_common::DinfraConfig for Test {
	type CIDEncodingLimit = frame_support::traits::ConstU32<16>;
	type Currency = Balances;
}

impl mocked_provider::Config for Test {
	type ProviderDeposit = frame_support::traits::ConstU64<FLAT_RATE>;
}

impl crate::Config for Test {
	type RuntimeCall = RuntimeCall;
	type RuntimeEvent = RuntimeEvent;
	type SubscriptionDeposit = frame_support::traits::ConstU64<5>;
	type MaxSelectedProviders = frame_support::traits::ConstU32<3>;
	type Scheduler = Scheduler;
	type Preimages = Preimage;
	type Provider = Provider;
	type AwardProcessWaitTime = frame_support::traits::ConstU32<5>;
}

// Build genesis storage according to the mock runtime.
// Add balances: providers 1-5, subscribers 101-105
pub fn new_test_ext() -> sp_io::TestExternalities {
	let mut t = system::GenesisConfig::<Test>::default().build_storage().unwrap();
	pallet_balances::GenesisConfig::<Test> {
		balances: vec![
			(1, PROVIDER_FUNDING),
			(2, PROVIDER_FUNDING),
			(3, PROVIDER_FUNDING),
			(4, PROVIDER_FUNDING),
			(5, PROVIDER_FUNDING),
			(101, SUBSCRIBER_FUNDING),
			(102, SUBSCRIBER_FUNDING),
			(103, SUBSCRIBER_FUNDING),
			(104, SUBSCRIBER_FUNDING),
			(105, SUBSCRIBER_FUNDING),
		],
	}
	.assimilate_storage(&mut t)
	.unwrap();
	t.into()
}

pub fn next_block() {
	System::set_block_number(System::block_number() + 1);
	Scheduler::on_initialize(System::block_number());
}

pub fn run_to(n: u64) {
	while System::block_number() < n {
		next_block();
	}
}

pub fn run_next(n: u64) {
	run_to(System::block_number() + n)
}
