// Copyright 2023 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

use codec::{Decode, Encode, MaxEncodedLen};
use dinfra_common::traits::Rate;
use scale_info::TypeInfo;
use sp_runtime::RuntimeDebug;

#[derive(PartialEq, Eq, Clone, Encode, Decode, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub enum TerminationReason {
	/// The subscription came to an end after funding was exhausted
	TerminatedByNetwork,
	/// The subscription was terminated at subscriber request,
	TerminatedBySubscriber,
	/// The subscription was terminated at provider request,
	TerminatedByProvider,
}

/// Preference for the awarding process of a subscription. Whether the subscriber wants given Offering or we
/// wants to start a selector based awarding process.
#[derive(PartialEq, Eq, Clone, Encode, Decode, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub enum AwardPreference<ProviderSelector, Provider> {
	/// The subscriber wants a concrete provider
	Explicit { provider: Provider },
	/// The subscriber wants to open a provider matching process according to preference.
	Selection { selector: ProviderSelector },
}

/// The Subscription state.
#[derive(PartialEq, Eq, Clone, Encode, Decode, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub enum SubscriptionState<ProviderAccount> {
	/// The Subscription has just been created and the network is in the process of awarding it to a Provider
	Created,
	/// The Subscription cannot be awarded to any provider
	Rejected,
	/// The Subscription has been awarded to a provider and the deployment process is underway
	Awarded { provider: ProviderAccount },
	/// The Subscription has reportedly been deployed by the provider
	Deployed { provider: ProviderAccount },
	/// The Subscription has been terminated a reason is provided.
	Terminated { provider: ProviderAccount, reason: TerminationReason },
	/// The Subscription was cancelled before being awarded
	Cancelled { reason: TerminationReason },
}

/// The subscription represents a requests by an infrastructure consumer for a deployment
#[derive(PartialEq, Eq, Clone, Encode, Decode, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub struct SubscriptionInfo<DeploymentDescriptor, DeploymentType, AwardPreference> {
	/// The requested deployment
	pub deployment_descriptor: DeploymentDescriptor,
	/// The deployment type
	pub deployment_type: DeploymentType,
	/// The requested awarding process for this deployment
	pub award_preference: AwardPreference,
}

/// This structure tracks the funding status of a subscription
/// Funds have been locked, may have been started, past payments may have been cleared
/// but most important fields are the remaining funds and price information which are
/// used to calculate and schedule termination.
#[derive(PartialEq, Eq, Clone, Encode, Decode, RuntimeDebug, TypeInfo, MaxEncodedLen)]
pub struct SubscriptionFunding<BlockNumber, Balance> {
	/// the block number at which the subscription was deployed, if it has been deployed
	pub deployed_at: Option<BlockNumber>,
	/// until when was this paid, it is never less than deployed_at if set. if there is a
	/// settlement at mid-life of the subscription this is updated to track how long the
	/// remaining funds will last. Also, rate changes require a settlement to date.
	pub paid_until: Option<BlockNumber>,
	/// Remaining locked funds available for the subscription
	pub remaining_funds: Balance,
	/// The rate used
	pub rate: Rate<Balance>,
}
