# DINFRA Subscription pallet

Responsible for managing infrastructure subscriptions.

Note that there is a one to one relationship between Account-ID and Subscription (also referred as subscriber). That means that one account ID can only request one subscription.

# Concepts
- Award process: the process by which subscriptions are awarded to providers taking into consideration the network state: preferences, capacity, rates, etc.
- Provider Selector: are a set of rules created to declare provider preferences. For example a subscriber may create a selector with a fixed set of providers, and DINFRA will award to one of the selected while observing load balance.
- termination: the process of terminating a subscription, which may be triggered by subscriber, provider or network.
- settlement: the network reserves balances for subscriptions and keeps tracks of block numbers. Settlements will actually transfer funds. It is possible to request settlements during the lifecycle of a subscription, some types of terminations will trigger a settelment.
- deletion: subscriptions are not automatically deleted on termination, this allows the parties to review them before deletion, for example, to learn what was the termination reason.

The pallet is self documented, and most functionalities are unit-tested, check both for details.

